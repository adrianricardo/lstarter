
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/** ------------------------------------------
 *  Route model binding
 *  ------------------------------------------
 */


Route::model('user', 'User');
Route::model('comment', 'Comment');
Route::model('post', 'Post');
Route::model('role', 'Role');
Route::model('order', 'Order');
Route::model('product', 'Product');

/** ------------------------------------------
 *  Route constraint patterns
 *  ------------------------------------------
 */
Route::pattern('comment', '[0-9]+');
Route::pattern('post', '[0-9]+');
Route::pattern('user', '[0-9]+');
Route::pattern('role', '[0-9]+');
Route::pattern('order', '[0-9]+');
Route::pattern('token', '[0-9a-z]+');

/* Subdomain routing */

Route::group(array('domain' => '{username}.lstarter.loc'), function() {
	Route::controller('checkout', 'CheckoutController');
	Route::get('/', 'UserController@getProfile');
	Route::get('return-policy', function()
	{
		// Return about us page
		return View::make('site/return-policy', array('layout' => 'profile.layouts.default'));
	});
});

Route::group(array('domain' => '{username}.murch.co'), function() {
	Route::controller('checkout', 'CheckoutController');
	Route::get('/', 'UserController@getProfile');
	Route::get('return-policy', function()
	{
		// Return about us page
		return View::make('site/return-policy', array('layout' => 'profile.layouts.default'));
	});
});



/** ------------------------------------------
 *  Admin Routes
 *  ------------------------------------------
 */
Route::group(array('prefix' => 'admin', 'before' => 'auth'), function()
{

	# Comment Management
	Route::get('comments/{comment}/edit', 'AdminCommentsController@getEdit');
	Route::post('comments/{comment}/edit', 'AdminCommentsController@postEdit');
	Route::get('comments/{comment}/delete', 'AdminCommentsController@getDelete');
	Route::post('comments/{comment}/delete', 'AdminCommentsController@postDelete');
	Route::controller('comments', 'AdminCommentsController');

	# Blog Management
	Route::get('blogs/{post}/show', 'AdminBlogsController@getShow');
	Route::get('blogs/{post}/edit', 'AdminBlogsController@getEdit');
	Route::post('blogs/{post}/edit', 'AdminBlogsController@postEdit');
	Route::get('blogs/{post}/delete', 'AdminBlogsController@getDelete');
	Route::post('blogs/{post}/delete', 'AdminBlogsController@postDelete');
	Route::controller('blogs', 'AdminBlogsController');

	# User Management
	Route::get('users/{user}/show', 'AdminUsersController@getShow');
	Route::get('users/{user}/edit', 'AdminUsersController@getEdit');
	Route::post('users/{user}/edit', 'AdminUsersController@postEdit');
	Route::get('users/{user}/delete', 'AdminUsersController@getDelete');
	Route::post('users/{user}/delete', 'AdminUsersController@postDelete');
	Route::controller('users', 'AdminUsersController');

	# User Role Management
	Route::get('roles/{role}/show', 'AdminRolesController@getShow');
	Route::get('roles/{role}/edit', 'AdminRolesController@getEdit');
	Route::post('roles/{role}/edit', 'AdminRolesController@postEdit');
	Route::get('roles/{role}/delete', 'AdminRolesController@getDelete');
	Route::post('roles/{role}/delete', 'AdminRolesController@postDelete');
	Route::controller('roles', 'AdminRolesController');

	# Order Management
	Route::controller('orders', 'AdminOrdersController');

	# Product Management
	Route::controller('products', 'AdminProductsController');

	# Prices management
	Route::controller('prices','AdminPricesController');

	# Designs management
	Route::controller('designs','AdminDesignsController');

	# Admin Dashboard
	Route::controller('/', 'AdminDashboardController');
});

/** ------------------------------------------
 *  Seller Routes
 *  ------------------------------------------
 */
Route::group(array('prefix' => 'dashboard', 'before' => 'seller'), function()
{

	# Product Management
	Route::controller('products', 'SellerProductsController');
	Route::controller('orders', 'SellerOrdersController');

	Route::get('get-paid', array('as' => 'get-paid','uses' => 'SellerDashboardController@getGetPaid'));

	# Seller Dashboard
	Route::controller('/', 'SellerDashboardController');
});




/* AUTH */

Route::get('/auth', function()
{
	if (Auth::guest()) {
		return Response::json( false );
	}else{
		return Response::json( Auth::user()->id );
	}
});



/** ------------------------------------------
 *  API Routes
 *  ------------------------------------------
 */
Route::group(array('prefix' => 'api', 'before' => 'auth'), function()
{
	Route::resource('productCategories', 'ProductCategoryApiController');
	Route::resource('garmentColors', 'GarmentColorApiController');
	Route::resource('products', 'ProductApiController');
});

Route::group(array('prefix' => 'api'), function()
{
	Route::resource('garments', 'GarmentApiController');
});


/** ------------------------------------------
 *  Frontend Routes
 *  ------------------------------------------
 */

// User reset routes
Route::get('user/reset/{token}', 'UserController@getReset');
// User password reset
Route::post('user/reset/{token}', 'UserController@postReset');
//:: User Account Routes ::
Route::post('user/{user}/edit', 'UserController@postEdit');

//:: User Account Routes ::
Route::post('user/login', 'UserController@postLogin');

# User RESTful Routes (Login, Logout, Register, etc)
Route::controller('user', 'UserController');



# Product Management
Route::controller('/products', 'ProductController');

# Checkout
Route::controller('/checkout', 'CheckoutController');


//:: Application Routes ::

# Filter for detect language
Route::when('contact-us','detectLang');

# Contact Us Static Page
Route::get('terms-of-service', function()
{
	// Return about us page
	return View::make('site/user/terms');
});

# Contact Us Static Page
Route::get('contact-us', function()
{
	// Return about us page
	return View::make('site/contact-us');
});

# Contact Us Static Page
Route::get('return-policy', function()
{
	// Return about us page
	return View::make('site/return-policy', array('layout' => 'site.layouts.default'));
});

Route::get('/signup', array('before' => 'detectLang','uses' => 'BetaSignupController@getSignup'));
Route::get('/thank-you', array('before' => 'detectLang','uses' => 'BetaSignupController@getThankYou'));
Route::get('/login', array('before' => 'detectLang','uses' => 'BetaSignupController@getLogin'));
Route::post('/signup', array('before' => 'detectLang','uses' => 'BetaSignupController@postSignup'));


# Seller Profiles 
Route::get('{username}', 'UserController@getProfile');

# Posts - Second to last set, match slug
Route::get('{postSlug}', 'BlogController@getView');
Route::post('{postSlug}', 'BlogController@postView');

# Index Page - Last route, no matches
Route::get('/', array('before' => 'detectLang','uses' => 'BetaSignupController@getIndex'));
Route::post('/', array('before' => 'detectLang','uses' => 'BetaSignupController@postIndex'));

if (!Auth::guest() && Auth::user()->hasRole('seller')) {
	Route::get('/', array('before' => 'seller','uses' => 'SellerProductsController@getIndex'));
}