<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class DownloadProductCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'downloadProduct';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		//
		$productID = $this->ask('Enter garment ID to download ');
		$categoryID = $this->ask('Enter garment category ');

		Shirtsio::setApiKey('6e0ad2e8221b2a118187787239df27132927a887');
		$product = Products::get_product($productID);

		$new_product = new Garment;
		$new_product->vendor_product_id = $productID;
		$new_product->garment_category_id = $categoryID;
		$new_product->name = $product['name'];
		$new_product->brand = $product['brand'];
		$new_product->style_id = $product['style_id'];
		$new_product->description = $product['description'];
		$new_product->materials = $product['materials'];
		$new_product->comments = $product['comments'];
		$new_product->active = 1;

		if($new_product->save()){
			foreach ($product['colors'] as $color) {
				$new_color = new GarmentColor;
				$new_color->garment_id = $productID;
				$new_color->name = $color['name'];
				$new_color->color = '#'.$color['hex'];
				$new_color->smallest = $color['smallest'];
				$new_color->largest = $color['largest'];
				$new_color->front_image = $color['front_image'];
				$new_color->back_image = $color['back_image'];
				$new_color->left_image = $color['left_image'];
				$new_color->right_image = $color['right_image'];
				$new_color->save();
			}

			$this->call('quoteProducts');

			return $this->info('Product: '.$product['name']. ' added successfully');
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('productID', InputArgument::REQUIRED, 'Shirts.io garment ID.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
