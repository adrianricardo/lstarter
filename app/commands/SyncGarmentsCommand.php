<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SyncGarmentsCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'syncGarments';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'sync garments';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->syncCategories();
		$this->syncGarments();
	}

	/**
	 * Get all garment categories from vendor. If exists, skip. If not save to db.
	 */
	public function syncCategories(){

		$categories = Curl::get('https://api.scalablepress.com/v2/categories');
		$this->info('Syncing garment categories..');
		$categories_updated = false;
		foreach ($categories->body as $category) {
			if($category->type == "Garment"){
				if(!(GarmentCategory::where('vendor_categoryId',$category->categoryId)->first())){
					$categories_updated = true;
					$new_category = new GarmentCategory;
					$new_category->name = $category->name;
					$new_category->vendor_categoryId= $category->categoryId;
					$new_category->family = $category->family;
					$new_category->url = $category->url;
					$new_category->save();
					$this->info('Saved new category '.$category->name);
				}
			}
		}
		if(!$categories_updated){
			$this->info('No categories updated.');
		}
	}

	public function syncGarments(){

		$continue = $this->ask('sync all garments? ');

		if(!($continue == "y" || $continue == "Y" || $continue == "yes")){
			return $this->info('Aborting.');
		}

		$categories = GarmentCategory::all();
		$this->info('Syncing garments..');

		foreach ($categories as $category) {
			if(!empty($category->url)){

				//get products
				$products = Curl::get($category->url)->body;

				foreach ($products->products as $product) {
					//if garment doesn't already exist in db
					if(!(Garment::where('vendor_product_id',$product->id)->first())){ 

						$product_data = Curl::get($product->url)->body;

						$new_product = new Garment;
						$new_product->vendor_product_id = $product->id;
						$new_product->garment_category_id = $category->vendor_categoryId;
						$new_product->name = $product_data->name;
						$new_product->brand = $product_data->properties->brand;
						$new_product->style_id = $product_data->properties->style;
						$new_product->description = $product_data->description;
						$new_product->materials = $product_data->properties->material;
						$new_product->comments = $product_data->comments;
						$new_product->available = $product_data->available;
						$new_product->active = 0;

						$new_product->availabilityUrl = $product_data->availabilityUrl;
						$new_product->url = $product_data->url;
						$new_product->image = (!empty($product_data->image)) ? $product_data->image->url : null;

						if($new_product->save()){
							$this->info('new garment '.$product->name);
							foreach ($product_data->colors as $color) {
								$sizes_count = count($color->sizes);

								$new_color = new GarmentColor;
								$new_color->garment_id = $new_product->id;
								$new_color->name = $color->name;
								$new_color->color = '#'.$color->hex;
								$new_color->sizes = json_encode($color->sizes);
								$new_color->vendor_product_id = $product->id;

								foreach ($color->images as $image) {
									if($image->label == "Front"){
										$new_color->front_image = $image->url;
									}
									if($image->label == "Back"){
										$new_color->back_image = $image->url;
									}
									if($image->label == "Right"){
										$new_color->right_image = $image->url;
									}
								}
								$new_color->save();
								$this->info('new garment color "'.$color->name.'" for '.$product->name);
							}

						}
							
						
					}

					// else update availability

				}
			}
		}

		$this->call('quoteProducts');
	}

}
