<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class QuoteProductsCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'quoteProducts';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Get base_price for all products that are missing it';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

		$garments = Garment::whereNull('base_price')->get();

		foreach ($garments as $garment) {

			$quote = PrintQuote::getBasePrice($garment->vendor_product_id);
			
			if(!$quote){
				$this->info('No colors avail for quote of '.$garment->vendor_product_id);
				continue;
			}
			
			$updated_product = Garment::find($garment->id);
			$updated_product->base_price = $quote['murch_price_per_shirt'];
			if($updated_product->save()){
				$this->info('Base price for #'.$garment->id. ' :$'.$quote['murch_price_per_shirt']);
			}
		}
		
		return $this->info('Done');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('productID', InputArgument::REQUIRED, 'Shirts.io garment ID.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
