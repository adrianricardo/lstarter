<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class DownloadAllProductsCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'downloadAllProducts';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Download all products for each category in db.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		//
		$continue = $this->ask('Download all products? ');

		if(!($continue == "y" || $continue == "Y" || $continue == "yes")){
			return $this->info('Aborting.');
		}

		Shirtsio::setApiKey('6e0ad2e8221b2a118187787239df27132927a887');

		$categories = GarmentCategory::all();

		foreach ($categories as $category) {
			//get products
			$products = Products::list_products($category['vendor_categoryId']);

			foreach ($products as $product) {

				$product_data = Products::get_product($product['product_id']);
			
				if(isset($product_data['brand'])){
					$new_product = new Garment;
					$new_product->vendor_product_id = $product['product_id'];
					$new_product->garment_category_id = $category['vendor_categoryId'];
					$new_product->name = $product_data['name'];
					$new_product->brand = $product_data['brand'];
					$new_product->style_id = $product_data['style_id'];
					$new_product->description = $product_data['description'];
					$new_product->materials = $product_data['materials'];
					$new_product->comments = $product_data['comments'];
					$new_product->active = 0;

					if($new_product->save()){
						foreach ($product_data['colors'] as $color) {
							$new_color = new GarmentColor;
							$new_color->garment_id = $new_product->id;
							$new_color->name = $color['name'];
							$new_color->color = '#'.$color['hex'];
							$new_color->smallest = $color['smallest'];
							$new_color->largest = $color['largest'];
							$new_color->front_image = (isset($color['front_image'])) ? $color['front_image'] : NULL;
							$new_color->back_image = (isset($color['back_image'])) ? $color['back_image'] : NULL;
							$new_color->left_image = (isset($color['left_image'])) ? $color['left_image'] : NULL;
							$new_color->right_image = (isset($color['right_image'])) ? $color['right_image'] : NULL;
							$new_color->save();
						}

						$this->info('Product: '.$product_data['name']. ' added successfully');
					}
				}else{
					$new_product = new Sublimable;
					$new_product->vendor_product_id = $product['product_id'];
					$new_product->garment_category_id = $category['vendor_categoryId'];
					$new_product->name = $product['name'];
					$new_product->print_size = $product['print_size'];
					$new_product->sio_price = (isset($product['price'])) ? $product['price'] : NULL;
					$new_product->weight = $product['weight'];
					$new_product->packaging_weight = (isset($product['packaging_weight'])) ? $product['packaging_weight'] : NULL;
					$new_product->active = 0;
					$new_product->save();

					$this->info('Sublimable: '.$product['name']. ' added successfully');
				}
			}
		}

		$this->call('quoteProducts');
		
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('productID', InputArgument::REQUIRED, 'Shirts.io garment ID.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
