<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class DownloadGarmentPhotosCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'downloadGarmentPhotos';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Download front,right,back photos for 
	each garment color. Save, and upload to s3 and update field name';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

		//first lets do the main garment image
		$garments = Garment::where('active',1)->where('image', 'LIKE', '%ooshirts%')->get();

		foreach ($garments as $garment) {
			//check main garment image
			$update = false;
			
			if (strpos($garment->image,'ooshirts') !== false) {

				//download photo
				$update = true;
				$updated_record = Garment::find($garment->id);
				try {
					$downloaded_image = file_get_contents($garment->image);
				}
				catch (Exception $e) {
					$this->info('caught error garment '.$garment->id);
					$pieces = explode("/", $garment->image);
					$count = count($pieces);
					$updated_record->image = $pieces[$count-1];
					$updated_record->save();
					continue;
				}
				
				$filenameOut = __DIR__ . '/images-test/image.jpg';
				$test = file_put_contents($filenameOut,$downloaded_image);

				//upload to s3
				$s3 = AWS::get('s3');
				$path = 'garment_colors/'.$garment->id.'/';
				$full_path = $path.$garment->vendor_product_id."-Main.jpg";

				$result = $s3->putObject(array(
					'Bucket'     => $_ENV['image_bucket'],
					'Key'        => $full_path,
					'SourceFile' => __DIR__ . '/images-test/image.jpg',
					'ContentType' => 'image/jpeg'
				));

				//update record
				$updated_record->image = $result['ObjectURL'];
				$updated_record->save();
				$this->info($garment->vendor_product_id . ' main downloaded');

				
			}
		}


		//now the garment color images
		$garments = Garment::where('active',1)->with('garmentColors')->whereHas('garmentColors', function($q)
		{
			$q->where('front_image', 'LIKE', '%ooshirts%')
				->orWhere('front_image', 'LIKE', '%ooshirts%')
				->orWhere('back_image', 'LIKE', '%ooshirts%')
				->orWhere('right_image', 'LIKE', '%ooshirts%');
		})->get();

	
		foreach ($garments as $garment) {
			foreach ($garment->GarmentColors as $color) {
				$update = false;
				$updated_record = GarmentColor::find($color->id);
				
				if (strpos($color->front_image,'ooshirts') !== false) {
					//download photo
					$update = true;
					try {
						$downloaded_image = file_get_contents($color->front_image);
					}
					catch (Exception $e) {
						$this->info('caught error garment '.$garment->id);
						$this->info('caught error color '.$color->id);
						$pieces = explode("/", $color->front_image);
						$count = count($pieces);
						$updated_record->front_image = $pieces[$count-1];
						$updated_record->save();
						continue;
					}
					
					$filenameOut = __DIR__ . '/images-test/image.jpg';
					$test = file_put_contents($filenameOut,$downloaded_image);


					//upload to s3
					$s3 = AWS::get('s3');
					$path = 'garment_colors/'.$color->garment_id.'/';
					$full_path = $path.$this->slugify($color->name)."-F.jpg";

					$result = $s3->putObject(array(
						'Bucket'     => $_ENV['image_bucket'],
						'Key'        => $full_path,
						'SourceFile' => __DIR__ . '/images-test/image.jpg',
						'ContentType' => 'image/jpeg'
					));

					//update record
					$updated_record->front_image = $result['ObjectURL'];
					$this->info($color->name . 'front downloaded');

					
				}

				if (strpos($color->back_image,'ooshirts') !== false) {
					//download photo
					try {
						$downloaded_image = file_get_contents($color->back_image);
					}
					catch (Exception $e) {
						$this->info('caught error garment '.$garment->id);
						$this->info('caught error color '.$color->id);
						$pieces = explode("/", $color->back_image);
						$count = count($pieces);
						$updated_record->back_image = $pieces[$count-1];
						$updated_record->save();
						continue;
					}
					
					$filenameOut = __DIR__ . '/images-test/image.jpg';
					$test = file_put_contents($filenameOut,$downloaded_image);


					//upload to s3
					$s3 = AWS::get('s3');
					$path = 'garment_colors/'.$color->garment_id.'/';
					$full_path = $path.$this->slugify($color->name)."-B.jpg";

					$result = $s3->putObject(array(
						'Bucket'     => $_ENV['image_bucket'],
						'Key'        => $full_path,
						'SourceFile' => __DIR__ . '/images-test/image.jpg',
						'ContentType' => 'image/jpeg'
					));

					//update record
					$updated_record->back_image = $result['ObjectURL'];
					$this->info($color->name . 'back downloaded');

					$update = true;
				}

				if (strpos($color->right_image,'ooshirts') !== false) {
					//download photo
					try {
						$downloaded_image = file_get_contents($color->right_image);
					}
					catch (Exception $e) {
						$this->info('caught error garment '.$garment->id);
						$this->info('caught error color '.$color->id);
						$pieces = explode("/", $color->right_image);
						$count = count($pieces);
						$updated_record->right_image = $pieces[$count-1];
						$updated_record->save();
						continue;
					}
					
					$filenameOut = __DIR__ . '/images-test/image.jpg';
					$test = file_put_contents($filenameOut,$downloaded_image);


					//upload to s3
					$s3 = AWS::get('s3');
					$path = 'garment_colors/'.$color->garment_id.'/';
					$full_path = $path.$this->slugify($color->name)."-R.jpg";

					$result = $s3->putObject(array(
						'Bucket'     => $_ENV['image_bucket'],
						'Key'        => $full_path,
						'SourceFile' => __DIR__ . '/images-test/image.jpg',
						'ContentType' => 'image/jpeg'
					));

					//update record
					$updated_record->right_image = $result['ObjectURL'];
					$this->info($color->name . 'right downloaded');

					$update = true;
				}

				if($update){
					$updated_record->save();
				}

				$this->info($garment->id);
			}
		}
		
	}


	public function slugify($text){ 
		// replace non letter or digits by -
		$text = preg_replace('~[^\\pL\d]+~u', '-', $text);

		// trim
		$text = trim($text, '-');

		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

		// lowercase
		$text = strtolower($text);

		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);

		if (empty($text))
		{
			return 'n-a';
		}

		return $text;
	}


}
