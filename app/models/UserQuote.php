<?php

use Illuminate\Support\Facades\URL; # not sure why i need this here :c

class UserQuote extends Eloquent{

	protected $table = 'user_quotes';


	protected $fillable = array(
		'user_id', 'count', 
		'sio_total','sio_subtotal','sio_shipping_price','sio_sales_tax','sio_discount',
		'sio_warnings','sio_garment_breakdown',
		'print_type','quote_summary',
		'created_at','updated_at'
	);


	public static function clearQuotes($user_id){
		DB::table('user_quotes')->where('user_id', '=', $user_id)->update(array('active' => 0));
	}


	public static function latest($user_id){
		$items = OrderItem::pendingPrint($user_id);
		$result = UserQuote::where("user_id", "=", $user_id)
			->orderBy("created_at", "desc")
			->where("count", "=", $items->sum('quantity'))
			->where("active",1)
			->first();
		return ($result) ? $result : false;
	}


	public static function QuoteUser($user_id)
	{
		//get order items for design ready products  that have not been submitted for printing
		$order_items = OrderItem::pendingPrint($user_id)->get();

		$quote_array = Array();
		$print_type = false;
		foreach ($order_items as $item) {

			$products = [];
			$products[] = array(
				'id' => $item->product->garment->vendor_product_id,
				'color' => $item->product->color,
				'quantity' => $item->quantity,
				'size' => $item->size,
			);
			$address = array(
				'name' => $item->order->shipping_name,
				'address1' => $item->order->shipping_street1,
				'address2' => $item->order->shipping_street2,
				'city' => $item->order->shipping_city,
				'state' => $item->order->shipping_state,
				'zip' => $item->order->shipping_zip,
				'country' => $item->order->shipping_country
			);
			$item_quote = Array(
				'type' => $item->product->type,
				'designId' => $item->product->design->vendor_designId,
				'products' => $products,
				'address' => $address
			);
			$quote_array['items'][] = $item_quote;

			if(!$print_type){
				$print_type = $item->product->design->type;
			}else{
				if($print_type != $item->product->design->type){
					$print_type = "Mixed";
				}
			}
		}

		//sd($quote_array);

		$options = Array(
			CURLOPT_USERPWD => ':'.Config::get('settings.sp_api')
		);
		$quote_url = "https://api.scalablepress.com/v2/quote/bulk";

		$count = $order_items->sum('quantity');

		//get quote
		try {
			$quote_resp = Curl::post($quote_url,$quote_array,$options)->body;
		} catch (Passioncoder\SimpleCurl\Exception $e) {
			print $e->getMessage();
			return false;
		}

		//sd($quote_resp); die();
		
		if(empty($quote_resp->total)){
			return false;
		}

		//get quote
		$adjustedPrices = AppHelper::adjustPrices($quote_resp,$count);

		$print_quote = New UserQuote;

		$print_quote->user_id            	= $user_id;
		$print_quote->count                 = $count;
		$print_quote->sio_subtotal          = $adjustedPrices['sio_subtotal'];
		$print_quote->sio_total             = $adjustedPrices['sio_total'];
		$print_quote->sio_price_per_shirt   = $adjustedPrices['sio_price_per_shirt'];
		$print_quote->murch_total           = $adjustedPrices['murch_total'];
		$print_quote->murch_price_per_shirt = $adjustedPrices['murch_price_per_shirt'];
		$print_quote->orderToken    		= $quote_resp->orderToken;
		$print_quote->sio_shipping_price    = $quote_resp->shipping;
		$print_quote->sio_warnings          = (empty($quote_resp->orderIssues)) ? "NULL" : json_encode($quote_resp->orderIssues);
		$print_quote->print_type            = $print_type;
		$print_quote->quote_summary         = json_encode($quote_array);

		$print_quote->save();

		return $adjustedPrices;
	}


	public static function getUserQuote($user_id = False){
		if(!$user_id){
			$user = Auth::user();
			$user_id = $user->id;
		}
		
		$product_ids = Product::where('user_id', $user_id)->get()->lists('id');

		$quotes = array();
		$quote = UserQuote::latest($user_id);
		if(empty($quote)){
			UserQuote::QuoteUser($user_id);
			$quote = UserQuote::latest($user_id);
		}
		if(!$quote){
			return 0;
		}
		$quote = array(
			'murch_total' => $quote->murch_total,
			'murch_price_per_shirt' => $quote->murch_price_per_shirt
		);
		return $quote;
	}


	/**
	 * Returns the date of the blog post creation,
	 * on a good and more readable format :)
	 *
	 * @return string
	 */
	public function created_at()
	{
		return $this->date($this->created_at);
	}

	/**
	 * Returns the date of the blog post last update,
	 * on a good and more readable format :)
	 *
	 * @return string
	 */
	public function updated_at()
	{
		return $this->date($this->updated_at);
	}

}
