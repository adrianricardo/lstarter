<?php

use Illuminate\Support\Facades\URL; # not sure why i need this here :c

class Seller extends Eloquent {

	protected $table = 'users';


	public static function payoutBalance(){
		$revenue = Seller::totalUnpaidRevenue();

		$cost = Seller::unpaidCost();

		//sd($cost);

		if($revenue > $cost){
			return $revenue - $cost;
		}else{
			return 0;
		}
	}
	
	public static function totalUnpaidRevenue(){
		$balance = 0;

		//sum order items price_at_sale where payout_id = NULL and sio_order_id NOT NULL
		$user = Auth::user();

		$products = Product::whereHas('unpaidOrderItems', function ($query) {
			$query->where('payout_id',NULL)->whereNotNull('sio_order_id');
		})->where('user_id', $user->id)->get();

		foreach ($products as $product) {
			foreach ($product->unpaidOrderItems as $item) {
				$balance += ($item->price_at_sale*$item->quantity);
			}
		} 
		
		return $balance;
	}


	public static function unpaidCost(){
		$unpaidCost = 0;

		//sum order items price_at_sale where payout_id = NULL and sio_order_id NOT NULL
		$user = Auth::user();

		$orders = User::with('pendingPayoutOrders')->where('id', $user->id)->get();

		//dd($products->Product); die();
		foreach ($orders as $order) {
			//dd($product);
			foreach ($order->pendingPayoutOrders as $print_order) {
				$unpaidCost += ($print_order->murch_total);
			}
		} 
		return $unpaidCost;
	}


	public static function createRecipient(){
		$token = $_POST['stripeToken'];
		$name = $_POST['name'];

		// Create a Recipient
		$recipient = Stripe_Recipient::create(array(
		  "name" => $name,
		  "type" => "individual",
		  "bank_account" => $token,
		  "email" => Auth::user()->email
		));

		//IF ACTIVE ACCOUNT
		$recipient = Stripe_Recipient::retrieve($recipient->id);
		if(!is_null($recipient->active_account)){
			$user_id = Auth::user()->id;
			$user = User::find($user_id);
			$user->recipient_id = $recipient->id;
			$user::$rules['username'] = 'required|unique:users,username,'.$user_id;
			$user::$rules['email'] = 'required|email|unique:users,email,'.$user_id;
			$user::$rules['password'] = 'required';
			if($user->update()){
				return true;
			}else{
				print_r($user->errors()->all()); die();
			}
			return true;
		}else{
			return false;
		}
	}


	public static function transferFunds(){
		$revenue = Seller::totalUnpaidRevenue();
		$cost = Seller::unpaidCost();
		$amount = $revenue-$cost;
		$user_id = Auth::user()->id;
		try {
			// Create a transfer to the specified recipient
			$transfer = Stripe_Transfer::create(array(
			  "amount" => ($amount*100), // amount in cents
			  "currency" => "usd",
			  "recipient" => Auth::user()->recipient_id,
			  "statement_description" => "Murch.co Payout")
			);

			//add payout to db
			$payout = new Payout;
			$payout->transfer_id = $transfer->id;
			$payout->user_id = $user_id;
			$payout->amount = $amount;
			$payout->recipient_id = Auth::user()->recipient_id;
			$payout->save();

			$payout_id = $payout->id;

			//update order items
			$items = OrderItem::pendingPayout($user_id);
			$items->update(array('order_items.payout_id' => $payout_id));

			//update sio_orders
			DB::Table('sio_orders')
				->where('user_id', $user_id)
				->where('sio_orders.payout_id', NULL)
				->update(array('sio_orders.payout_id' => $payout_id));

	

		}catch(Exception $e) {
			return $e;
		} 

		return true;
	}

}
