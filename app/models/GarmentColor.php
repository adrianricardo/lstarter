<?php

use Illuminate\Support\Facades\URL; # not sure why i need this here :c
use Robbo\Presenter\PresentableInterface;

class GarmentColor extends Eloquent {

	protected $table = 'garment_colors';

	public function garment()
	{
		return $this->belongsTo('Garment');
	}

	public static function getColorsForCategory($name){
		$garment_category_ids =  ProductGarmentCategory::where('name',$name)->lists('garment_category_id');
		$garment_ids = Garment::where('active',1)->whereIn('garment_category_id',$garment_category_ids)->lists('id');
		$garment_colors = GarmentColor::whereIn('garment_id',$garment_ids)->get();
		return $garment_colors;
	}

}
