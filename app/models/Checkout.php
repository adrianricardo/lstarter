<?php

use Illuminate\Support\Facades\URL; # not sure why i need this here :c
use Robbo\Presenter\PresentableInterface;

class Checkout extends Eloquent{

	protected $table = 'orders';

	protected $fillable = array(
		'product_id', 'quantity', 
		'billing_name','billing_street1','billing_street2','billing_city','billing_state',
		'billing_zip','billing_country',
		'shipping_name','shipping_street1','shipping_street2','shipping_city','shipping_state',
		'shipping_zip','shipping_country',
		'created_at','updated_at');


	/**
	* Returns shipping price for order. $2 per item after the first
	* + $2.25 per item for Canada
	* + $6.75 per item for any where else
	* @return decimal
	*/
	public static function calculateShipping(){
		if(!Session::has('transaction_data')){
			$international = 0;
		}else{
			$transaction_data = Session::get('transaction_data');
			switch ($transaction_data['stripeShippingAddressCountryCode']) {
				case 'US':
					$international = 0;
					break;
				case 'CA':
					$international = 2.25;
				default:
					$international = 6.75;
					break;
			}
		}
		return Config::get('settings.shipping') + ((Cart::count() - 1) * 2) + ($international * Cart::count());
	}

	public function saveOrder()
	{
		$user = Auth::user();
		if(empty($user->id)){
			$user_id = null;
		}else{
			$user_id = $user->id;
		}

		$cart = Cart::content();
		$total = Cart::total();
		$shipping = $this->calculateShipping();
		$amount = $total+$shipping;

		$transaction_data = Session::get('transaction_data');

		$order = new Checkout;
		$order->shipping            = $shipping;
		$order->subtotal            = $total;
		$order->total               = $amount;
		$order->user_id             = $user_id;
		$order->email 				= $transaction_data['stripeEmail'];
		$order->billing_name        = $transaction_data['stripeBillingName'];
		$order->billing_street1     = $transaction_data['stripeBillingAddressLine1'];
		$order->billing_street2     = null;
		$order->billing_city        = $transaction_data['stripeBillingAddressCity'];
		$order->billing_state      	= isset($transaction_data['stripeBillingAddressState']) ? $transaction_data['stripeBillingAddressState'] : null;
		$order->billing_zip         = $transaction_data['stripeBillingAddressZip'];
		$order->billing_country     = $transaction_data['stripeBillingAddressCountryCode']; 
		$order->shipping_name       = $transaction_data['stripeShippingName'];
		$order->shipping_street1    = $transaction_data['stripeShippingAddressLine1'];
		$order->shipping_street2    = null;
		$order->shipping_city       = $transaction_data['stripeShippingAddressCity'];
		$order->shipping_state      = isset($transaction_data['stripeShippingAddressState']) ? $transaction_data['stripeShippingAddressState'] : null;
		$order->shipping_zip        = $transaction_data['stripeShippingAddressZip'];
		$order->shipping_country    = $transaction_data['stripeShippingAddressCountryCode'];

		//save order
		$order->save();

		//return id
		$order_id = $order->id;

		foreach ($cart as $item) {
			
			//new order item
			$order_item = new OrderItem;
			//give data
			$order_item->order_id       = $order_id;
			$order_item->product_id     = $item->id;
			$order_item->quantity       = $item->qty;
			$order_item->size           = $item->options->has('size') ? $item->options->size : null;
			$order_item->price_at_sale  = $item->price;

			//save order items to database
			$order_item->save();

			
		}


		//destroy cart
		Cart::destroy();

		return $order_id;


	}

	/**
	 * Returns the date of the blog post creation,
	 * on a good and more readable format :)
	 *
	 * @return string
	 */
	public function created_at()
	{
		return $this->date($this->created_at);
	}

	/**
	 * Returns the date of the blog post last update,
	 * on a good and more readable format :)
	 *
	 * @return string
	 */
	public function updated_at()
	{
		return $this->date($this->updated_at);
	}

	public function getPresenter()
	{
		return new CheckoutPresenter($this);
	}
}
