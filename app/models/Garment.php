<?php

use Illuminate\Support\Facades\URL; # not sure why i need this here :c
use Robbo\Presenter\PresentableInterface;

class Garment extends Eloquent {


	public function getPresenter()
	{
		return new GarmentPresenter($this);
	}

	public function garmentColors()
	{
		return $this->hasMany('GarmentColors');
	}

	public static function getBaseCostColor($vendor_product_id){
		$garment = Garment::where('vendor_product_id',$vendor_product_id)->first();

		$color = GarmentColor::where('garment_id',$garment->id)->where('name','White')->first();

		if(!count($color)){
			$color = GarmentColor::where('garment_id',$garment->id)->first();

			if(!count($color)){
				return false;
			}
		}

		return $color['name'];
	}

}
