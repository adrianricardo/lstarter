<?php

use Illuminate\Support\Facades\URL; # not sure why i need this here :c

class Currency extends Eloquent {

	public static function getCurrencies(){
		$currency_array = ['USD','EUR','GBP'];
		$currencies = Currency::whereIn('code',$currency_array)->get();

		if($currencies->isEmpty() || (time() - $currencies->first()->pluck('timestamp')) > (6*60*60)){ //6 hours

			$file = 'latest.json';
			$appId = 'd8b46cfc0ec84360acdec533930ec049';

			// Open CURL session:
			$ch = curl_init("http://openexchangerates.org/api/{$file}?app_id={$appId}");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			// Get the data:
			$json = curl_exec($ch);
			curl_close($ch);

			// Decode JSON response:
			$exchangeRates = json_decode($json);

			Currency::truncate();

			foreach ($exchangeRates->rates as $code => $rate) {
				$new_currency = new Currency;
				$new_currency->code = $code;
				$new_currency->rate = $rate;
				$new_currency->timestamp = $exchangeRates->timestamp;
				$new_currency->save();
			}

			$currencies = Currency::whereIn('code',$currency_array)->get();
		}

		return $currencies;
	}
}
