<?php

use Illuminate\Support\Facades\URL; # not sure why i need this here :c

class PrintOrder extends Eloquent{

    protected $table = 'sio_orders';
    
    /* Relationships */
    public function payout()
    {
        return $this->belongsTo('Payout');
    }

    protected $fillable = array(
        'quote_id','orderToken',
        'orderId',
        'total',
        'shipping',
        'subtotal',
        'total',
        'murch_total',
        'vendor_createdAt',
        'created_at','updated_at');

    public static function Order($user_id)
    {

        $latest = UserQuote::latest($user_id);
        $order_array['orderToken'] = $latest->orderToken;

        $options = Array(
            CURLOPT_USERPWD => ':'.Config::get('settings.sp_api')
        );
        $api_url = "https://api.scalablepress.com/v2/order";

        //get quote
        try {
            $order_resp = Curl::post($api_url,$order_array,$options)->body;
        } catch (Passioncoder\SimpleCurl\Exception $e) {
            print $e->getMessage();
            return false;
        }

        if(property_exists($order_resp, 'statusCode') && $order_resp->statusCode == 400){
            //There was an ERROR
            sd($order_resp);
        }


        DB::table('sio_orders')->insert(
            array(
                'user_id' => $user_id, 
                'quote_id' => $latest->id,
                'orderToken' => $order_resp->orderToken,
                'orderId' => $order_resp->orderId,
                'order_summary' => json_encode($order_resp->items),
                'shipping' => $order_resp->shipping,
                'total' => $order_resp->total,
                'murch_total' => $latest->murch_total,
                'subtotal' => $order_resp->subtotal,
                'vendor_createdAt' => $order_resp->createdAt
        ));  

        //updated submitted date on orders
        $product_ids = Product::where('user_id', $user_id)->get()->lists('id');
        OrderItem::whereIn('product_id',$product_ids)
            ->whereHas('product', function($q) {
                $q->whereNotNull('design_id');
            })
            ->whereNull('sio_order_id')
            ->update(array('sio_order_id'=>$order_resp->orderId));
    }

    /**
     * Returns the date of the blog post creation,
     * on a good and more readable format :)
     *
     * @return string
     */
    public function created_at()
    {
        return $this->date($this->created_at);
    }

    /**
     * Returns the date of the blog post last update,
     * on a good and more readable format :)
     *
     * @return string
     */
    public function updated_at()
    {
        return $this->date($this->updated_at);
    }
}
