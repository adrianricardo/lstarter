<?php

use Illuminate\Support\Facades\URL; # not sure why i need this here :c
use Robbo\Presenter\PresentableInterface;

class Design extends Eloquent {

	/* Relationships */
	public function sides()
	{
		return $this->hasMany('DesignSide');
	}
	public function products()
	{
		return $this->hasMany('Products');
	}

}
