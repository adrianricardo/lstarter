<?php

use Illuminate\Support\Facades\URL; # not sure why i need this here :c

class PrintQuote extends Eloquent{

	protected $table = 'sio_quotes';


	protected $fillable = array(
		'product_id', 'count', 
		'sio_total','sio_subtotal','sio_shipping_price','sio_sales_tax','sio_discount',
		'sio_warnings','sio_garment_breakdown',
		'print_type','quote_summary',
		'created_at','updated_at'
	);


	


	public static function getBasePrice($vendor_product_id){

		//get white or the first color
		$color = Garment::getBaseCostColor($vendor_product_id);

		if(!$color){
			return false;
		}

		$quote_array = Array(
			'type'=> 'dtg',
			'sides[front]'=> 1,
			'products[0][id]'=> $vendor_product_id,
			'products[0][color]'=> $color, 
			'products[0][quantity]'=> 1, 
			'products[0][size]' => 'med',
		);

		/*$quote_array = Array(
			'items[0][type]' => $product->type,
			'items[0][sides][front]' => 1,
			'items[0][products][0][id]' => $vendor_product_id,
			'items[0][products][0][color]' => $color,
			'items[0][products][0][quantity]' => '12',
			'items[0][products][0][size]' => 'med',
			'items[1][type]' => $product->type,
			'items[1][sides][front]' => 1,
			'items[1][products][0][id]' => 'gildan-sweatshirt-crew',
			'items[1][products][0][color]' => 'ash',
			'items[1][products][0][quantity]' => '12',
			'items[1][products][0][size]' => 'lrg'
		); */

		$options = Array(
			CURLOPT_USERPWD => ':'.Config::get('settings.sp_api')
		);

		//get quote
		try {
			$quote_resp = Curl::post('https://api.scalablepress.com/v2/quote',$quote_array,$options)->body;
		} catch (Passioncoder\SimpleCurl\Exception $e) {
			print $e->getMessage();
			return false;
		}

		if(empty($quote_resp->total)){
			return false;
		}

		$adjustedPrices = AppHelper::adjustPrices($quote_resp,1);

		return $adjustedPrices;
	}


	public static function Quote($product_id)
	{

		$order_items = OrderItem::where('product_id',$product_id)->with('Order')->whereNull('sio_order_id')->get();

		$has_orders = (count($order_items));

		$shirt_order_count = $num_shirts = DB::table('order_items')
		->where('product_id','=',$product_id)
		->whereNull('sio_order_id')
		->sum('quantity');

		$shirt_quote_count = ($shirt_order_count == 0) ? 1 : $shirt_order_count;

		$product = Product::where('id',$product_id)->with('garment')->with('design')->first();

		if(empty($product->design) || !$has_orders){ 
			//no design or orders so lets do quote-only scenario
			$quote_array = Array(
				'type' => $product->type,
				'sides[front]' => 1,
				'products[0][id]' => $product->garment->vendor_product_id,
				'products[0][color]' => $product->color,
				'products[0][quantity]' => '1',
				'products[0][size]' => 'med',
			); 
			$print_type = "dtg";
			$quote_url = "https://api.scalablepress.com/v2/quote";
		}else{ // we have a design and this product has received orders
			$quote_array = Array();
			foreach ($order_items as $order_item) {
				$products = [];
				$products[] = array(
					'id' => $product->garment->vendor_product_id,
					'color' => $product->color,
					'quantity' => $order_item->quantity,
					'size' => $order_item->size,
				);
				$address = array(
					'name' => $order_item->Order->shipping_name,
					'address1' => $order_item->Order->shipping_street1,
					'address2' => $order_item->Order->shipping_street2,
					'city' => $order_item->Order->shipping_city,
					'state' => $order_item->Order->shipping_state,
					'zip' => $order_item->Order->shipping_zip,
					'country' => $order_item->Order->shipping_country
				);
				$size_array = Array(
					'type' => $product->type,
					'designId' => $product->design->vendor_designId,
					'products' => $products,
					'address' => $address
				);
				$quote_array['items'][] = $size_array;
			}

			$print_type = $product->design->type;
			$quote_url = "https://api.scalablepress.com/v2/quote/bulk";

		}

		//sd($quote_array);

		$options = Array(
			CURLOPT_USERPWD => ':'.Config::get('settings.sp_api')
		);


		


		//get quote
		try {
			$quote_resp = Curl::post($quote_url,$quote_array,$options)->body;
		} catch (Passioncoder\SimpleCurl\Exception $e) {
			print $e->getMessage();
			return false;
		}

		//sd($quote_resp); die();
		
		if(empty($quote_resp->total)){
			return false;
		}

		//get quote
		$adjustedPrices = AppHelper::adjustPrices($quote_resp,$shirt_quote_count);

		$print_quote = New PrintQuote;

		$print_quote->product_id            = $product_id;
		$print_quote->count                 = $shirt_order_count;
		$print_quote->sio_subtotal          = $adjustedPrices['sio_subtotal'];
		$print_quote->sio_total             = $adjustedPrices['sio_total'];
		$print_quote->sio_price_per_shirt   = $adjustedPrices['sio_price_per_shirt'];
		$print_quote->murch_total           = $adjustedPrices['murch_total'];
		$print_quote->murch_price_per_shirt = $adjustedPrices['murch_price_per_shirt'];
		$print_quote->orderToken    		= $quote_resp->orderToken;
		$print_quote->sio_shipping_price    = $quote_resp->shipping;
		$print_quote->sio_warnings          = (empty($quote_resp->orderIssues)) ? "NULL" : json_encode($quote_resp->orderIssues);
		$print_quote->print_type            = $print_type;
		$print_quote->quote_summary         = json_encode($quote_array);

		$print_quote->save();

		return $adjustedPrices;
	}


	public static function getQuotes(){
		$user = Auth::user();
		$products = $products = Product::where('user_id', $user->id)->get();

		$quotes = array();
		//foreach product, check if the quote is current
		foreach ($products as $product) {
			$quote = PrintQuote::where("product_id", "=", $product->id)
				->orderBy("created_at", "desc")
				->where("count", "=", DB::table("order_items")
					->where("product_id", "=", $product->id)
					->where("sio_order_id", "=", null)
					->sum("quantity"))
				->first();
			if(empty($quote)){
				PrintQuote::Quote($product->id);
				$quote = PrintQuote::where("product_id", "=", $product->id)
				->orderBy("created_at", "desc")
				->where("count", "=", DB::table("order_items")->where("product_id", "=", $product->id)->where("sio_order_id", "=", null)->sum("quantity"))->first();
			}
			if(!empty($quote)){
				$quotes[$product->id] = array(
					'murch_total' => $quote->murch_total,
					'murch_price_per_shirt' => $quote->murch_price_per_shirt,
				);
			}else{
				Session::flash('info', 'Quote not availble Contact adrian@murch.co for help.');
			}
		}
		//die();
		return $quotes;
	}


	/**
	 * Returns the date of the blog post creation,
	 * on a good and more readable format :)
	 *
	 * @return string
	 */
	public function created_at()
	{
		return $this->date($this->created_at);
	}

	/**
	 * Returns the date of the blog post last update,
	 * on a good and more readable format :)
	 *
	 * @return string
	 */
	public function updated_at()
	{
		return $this->date($this->updated_at);
	}

}
