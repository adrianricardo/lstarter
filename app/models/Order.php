<?php

use Illuminate\Support\Facades\URL; # not sure why i need this here :c

class Order extends Eloquent{

    /* Relationships */
    public function orderItems()
    {
        return $this->hasMany('OrderItem');
    }


    /**
     * Returns the date of the order  creation,
     * on a good and more readable format :)
     *
     * @return string
     */
    public function created_at()
    {
        return $this->date($this->created_at);
    }

    /**
     * Returns the date of the order last update,
     * on a good and more readable format :)
     *
     * @return string
     */
    public function updated_at()
    {
        return $this->date($this->updated_at);
    }
}
