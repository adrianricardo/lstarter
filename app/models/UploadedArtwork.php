<?php

use Illuminate\Support\Facades\URL; # not sure why i need this here :c

class UploadedArtwork extends Eloquent{

    protected $fillable = array('product_id', 'description','created_at','updated_at');

    /**
     * Returns the date of the blog post creation,
     * on a good and more readable format :)
     *
     * @return string
     */
    public function created_at()
    {
        return $this->date($this->created_at);
    }

    /**
     * Returns the date of the blog post last update,
     * on a good and more readable format :)
     *
     * @return string
     */
    public function updated_at()
    {
        return $this->date($this->updated_at);
    }

    public function getPresenter()
    {
        return new ProductPresenter($this);
    }
}
