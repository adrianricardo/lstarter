<?php

use Illuminate\Support\Facades\URL; # not sure why i need this here :c
use Robbo\Presenter\PresentableInterface;

class Product extends Eloquent{

	protected $fillable = array('name', 'garment_id', 'user_id','type','color','print_type','price','created_at','updated_at');

	/* Relationships */
	public function orderItems()
	{
		return $this->hasMany('OrderItem');
	}
	public function unOrderedOrderItems()
	{
		return $this->hasMany('OrderItem')->where('payout_id',NULL)->where('sio_order_id',NULL);
	}
	public function unpaidOrderItems()
	{
		return $this->hasMany('OrderItem')->where('payout_id',NULL)->whereNotNull('sio_order_id');
	}
	public function design()
	{
		return $this->belongsTo('Design');
	}
	public function designSides()
	{
		return $this->design->hasMany('DesignSide'); 
	}
	public function garment()
	{
		return $this->belongsTo('Garment', 'garment_id');
	}
	public function printOrders()
	{
		return $this->hasMany('PrintOrder')->where('payout_id',NULL);
	}

	/**
	 * Returns the date of the blog post creation,
	 * on a good and more readable format :)
	 *
	 * @return string
	 */
	public function created_at()
	{
		return $this->date($this->created_at);
	}

	/**
	 * Returns the date of the blog post last update,
	 * on a good and more readable format :)
	 *
	 * @return string
	 */
	public function updated_at()
	{
		return $this->date($this->updated_at);
	}

	public function getPresenter()
	{
		return new ProductPresenter($this);
	}
}
