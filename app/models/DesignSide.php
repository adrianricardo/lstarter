<?php

use Illuminate\Support\Facades\URL; # not sure why i need this here :c
use Robbo\Presenter\PresentableInterface;

class DesignSide extends Eloquent {

	/* Relationships */
	public function sides()
	{
		return $this->belongsTo('Design');
	}

}
