<?php

use Illuminate\Support\Facades\URL; # not sure why i need this here :c

class OrderItem extends Eloquent{

	protected $table = 'order_items';

	protected $fillable = array('product_id', 'order_id','quantity','size');

	/* Relationships */
	public function product()
	{
		return $this->belongsTo('Product');
	}
	public function order()
	{
		return $this->belongsTo('Order');
	}
	public function payout()
	{
		return $this->belongsTo('Payout');
	}

	public static function revenueFromCurrentBatch(){
		$revenue = 0;
		$order_items = OrderItem::pendingPrint()->get();
		foreach ($order_items as $item) {
			$revenue += $item->price_at_sale*$item->quantity;
		}
		return $revenue;
	}

	public static function pendingPayout($user_id = False){
		if(!$user_id){
			$user_id = Auth::user()->id;
		}

		$product_ids = Product::where('user_id', $user_id)->get()->lists('id');

		if(!$product_ids){
			$product_ids = [0];
		}

		return OrderItem::whereIn('product_id',$product_ids)
			->with('Order')
			->whereHas('product', function($q) {
				$q->whereNotNull('design_id');
			})
			->with(array('product.design','product.garment'))
			->whereNotNull('sio_order_id')
			->whereNull('payout_id');
	}


	public static function pendingPrint($user_id = False){
		if(!$user_id){
			$user_id = Auth::user()->id;
		}

		$product_ids = Product::where('user_id', $user_id)->get()->lists('id');

		if(!$product_ids){
			$product_ids = [0];
		}

		return OrderItem::whereIn('product_id',$product_ids)
			->with('Order')
			->whereHas('product', function($q) {
				$q->whereNotNull('design_id');
			})
			->with(array('product.design','product.garment'))
			->whereNull('payout_id')
			->whereNull('sio_order_id');
	}
	

	/**
	 * Returns the date of the blog post creation,
	 * on a good and more readable format :)
	 *
	 * @return string
	 */
	public function created_at()
	{
		return $this->date($this->created_at);
	}

	/**
	 * Returns the date of the blog post last update,
	 * on a good and more readable format :)
	 *
	 * @return string
	 */
	public function updated_at()
	{
		return $this->date($this->updated_at);
	}
}
