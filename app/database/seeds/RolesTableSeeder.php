<?php

class RolesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('roles')->delete();

        $adminRole = new Role;
        $adminRole->name = 'admin';
        $adminRole->save();

        $commentRole = new Role;
        $commentRole->name = 'comment';
        $commentRole->save();

        $sellerRole = new Role;
        $sellerRole->name = 'seller';
        $sellerRole->save();

       

        $user = User::where('username','=','user')->first();
        $user->attachRole( $commentRole );

        $seller = User::where('username','=','seller')->first();
        $seller->attachRole( $sellerRole );

        $user = User::where('username','=','admin')->first();
        $user->attachRole( $adminRole );
        $user->attachRole( $sellerRole );

    }

}
