<?php

class UsersTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();


        $users = array(
            array(
                'username'      => 'admin',
                'email'      => 'admin@example.org',
                'password'   => Hash::make('admin'),
                'confirmed'   => 1,
                'confirmation_code' => md5(microtime().Config::get('app.key')),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'display_name' => NULL,
                'image' => NULL,
            ),
            array(
                'username'      => 'user',
                'email'      => 'user@example.org',
                'password'   => Hash::make('user'),
                'confirmed'   => 1,
                'confirmation_code' => md5(microtime().Config::get('app.key')),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'display_name' => NULL,
                'image' => NULL,
            ),
            array(
                'username'      => 'seller',
                'email'      => 'seller@example.org',
                'password'   => Hash::make('seller'),
                'confirmed'   => 1,
                'confirmation_code' => md5(microtime().Config::get('app.key')),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'display_name' => 'Seller name',
                'image' => 'https://s3.amazonaws.com/dev_murch/u/3.png',
            )
        );

        $options = array(
            array(
                'user_id'      => 3,
                'show_display_name'   => 1
            )
        );

        DB::table('users')->insert( $users );
        DB::table('store_options')->insert( $options );
    }

}
