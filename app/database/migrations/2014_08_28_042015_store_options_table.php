<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StoreOptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_options', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->boolean('show_display_name')->default(1);
			$table->boolean('currency_converter')->default(0);
			$table->text('custom_css')->nullable()->default(null);
			$table->mediumText('background_image')->nullable()->default(null);
			$table->text('thank_you_message')->nullable()->default(null);
			$table->string('support_email')->nullable()->default(null);
			$table->timestamps();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('store_options', function($t) {
			$t->dropForeign('store_options_user_id_foreign');
	});
		Schema::drop('store_options');
	}

}
