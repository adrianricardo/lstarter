<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetaSignup extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('beta_signups', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('email');
			$table->string('artist_name')->nullable();
			$table->string('artist_twitter')->nullable();
			$table->integer('has_manager')->nullable();
			$table->string('manager_name')->nullable();
			$table->string('manager_email')->nullable();
			$table->string('manager_phone')->nullable();
			$table->string('manager_twitter')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('beta_signups');
	}

}
