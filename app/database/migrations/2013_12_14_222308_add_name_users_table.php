<?php

use Illuminate\Database\Migrations\Migration;

class AddNameUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function($t) {
			$t->engine = 'InnoDB';
        	$t->string('display_name', 150)->nullable();
        	$t->string('image')->nullable();
        	$t->string('facebook', 150)->nullable();
        	$t->string('twitter', 150)->nullable();
        	$t->string('instagram', 150)->nullable();
        	$t->integer('agreed_to_terms')->default(0);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function($t) {
			$t->dropColumn('display_name');
			$t->dropColumn('image');
			$t->dropColumn('facebook');
        	$t->dropColumn('twitter');
        	$t->dropColumn('instagram');
        });
	}

}