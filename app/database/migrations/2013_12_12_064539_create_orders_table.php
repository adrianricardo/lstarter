<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->decimal('subtotal', 8, 2);
			$table->decimal('total', 8, 2);
			$table->decimal('shipping', 8, 2);
			$table->integer('user_id')->nullable();
			$table->string('email')->nullable();
			$table->string('billing_name')->nullable();
			$table->string('billing_street1')->nullable();
			$table->string('billing_street2')->nullable();
			$table->string('billing_city')->nullable();
			$table->string('billing_state')->nullable();
			$table->string('billing_zip')->nullable();
			$table->string('billing_country')->nullable();
			$table->string('shipping_name')->nullable();
			$table->string('shipping_street1')->nullable();
			$table->string('shipping_street2')->nullable();
			$table->string('shipping_city')->nullable();
			$table->string('shipping_state')->nullable();
			$table->string('shipping_zip')->nullable();
			$table->string('shipping_country')->nullable();

			$table->timestamps();
		});

		Schema::create('order_items', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('order_id')->unsigned();
			$table->integer('product_id')->unsigned();
			$table->integer('quantity');
			$table->string('size')->nullable();
			$table->decimal('price_at_sale', 8, 2);
			$table->string('sio_order_id')->nullable();
			$table->timestamps();
			$table->foreign('order_id')
		      ->references('id')->on('orders')
		      ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_items');
		Schema::drop('orders');
	}

}
