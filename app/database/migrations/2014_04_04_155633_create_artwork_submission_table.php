<?php

use Illuminate\Database\Migrations\Migration;

class CreateArtworkSubmissionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('uploaded_artworks', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('product_id');
			$table->string('file');
			$table->text('description')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('uploaded_artworks');
	}

}