<?php

use Illuminate\Database\Migrations\Migration;

class CreateShirtsioTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sio_orders', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('quote_id');
			$table->string('orderToken');
			$table->string('orderId');
			$table->decimal('murch_total', 9, 2);
			$table->decimal('total', 9, 2);
			$table->decimal('shipping', 9, 2);
			$table->decimal('subtotal', 9, 2);
			$table->string('vendor_createdAt');
			$table->text('order_summary');
			$table->timestamps();
			$table->foreign('user_id')->references('id')->on('users');
		});

		Schema::create('sio_quotes', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('product_id')->unsigned();
			$table->integer('count')->nullable();
			$table->decimal('sio_subtotal', 9, 2);
			$table->decimal('sio_total', 9, 2);
			$table->decimal('sio_price_per_shirt', 9, 2);
			$table->decimal('murch_total', 9, 2);
			$table->decimal('murch_price_per_shirt', 9, 2);
			$table->decimal('sio_shipping_price', 9, 2);
			$table->string('orderToken')->nullable();
			$table->text('sio_warnings')->nullable();
			$table->text('sio_garment_breakdown')->nullable();
			$table->string('print_type');
			$table->text('quote_summary');
			$table->foreign('product_id')->references('id')->on('products');
			$table->timestamps();
		});

		Schema::create('user_quotes', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('count')->nullable();
			$table->boolean('active')->default(true);
			$table->decimal('sio_subtotal', 9, 2);
			$table->decimal('sio_total', 9, 2);
			$table->decimal('sio_price_per_shirt', 9, 2);
			$table->decimal('murch_total', 9, 2);
			$table->decimal('murch_price_per_shirt', 9, 2);
			$table->decimal('sio_shipping_price', 9, 2);
			$table->string('orderToken');
			$table->text('sio_warnings')->nullable();
			$table->text('sio_garment_breakdown')->nullable();
			$table->string('print_type');
			$table->text('quote_summary');
			$table->foreign('user_id')->references('id')->on('users');
			$table->timestamps();
		});

		Schema::create('garment_categories', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('vendor_categoryId');
			$table->string('name');
			$table->string('family');
			$table->string('url');
			$table->timestamps();
		});

		Schema::create('garments', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('vendor_product_id');
			$table->string('garment_category_id');
			$table->string('name');
			$table->string('brand');
			$table->string('style_id');
			$table->text('description');
			$table->text('description_custom')->nullable();
			$table->text('materials');
			$table->text('comments');
			$table->boolean('active');
			$table->boolean('available');
			$table->string('availabilityUrl');
			$table->string('url');
			$table->string('image')->nullable();
			$table->decimal('base_price', 9, 2)->nullable()->default(NULL);
			$table->timestamps();
		});

		Schema::create('sublimables', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('vendor_product_id');
			$table->integer('garment_category_id')->unsigned();
			$table->string('name');
			$table->string('print_size');
			$table->decimal('sio_price', 9, 2)->nullable()->default(NULL);
			$table->decimal('weight', 9, 2);
			$table->decimal('packaging_weight', 9, 2)->nullable()->default(NULL);
			$table->integer('active');
			$table->decimal('base_price', 9, 2)->nullable()->default(NULL);
			$table->timestamps();
			$table->foreign('garment_category_id')->references('id')->on('garment_categories');
		});

		Schema::table('products', function($t) {
			$t->foreign('garment_id')->references('id')->on('garments');
		});

		Schema::create('garment_colors', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('garment_id')->unsigned();
			$table->string('vendor_product_id');
			$table->string('name');
			$table->string('color');
			$table->string('smallest')->nullable();
			$table->string('largest')->nullable();
			$table->string('sizes');
			$table->string('front_image')->nullable()->default(NULL);
			$table->string('back_image')->nullable()->default(NULL);
			$table->string('left_image')->nullable()->default(NULL);
			$table->string('right_image')->nullable()->default(NULL);
			$table->timestamps();
			$table->foreign('garment_id')->references('id')->on('garments');
		});
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('products', function($t) {
			$t->dropForeign('products_garment_id_foreign');
		});

		Schema::table('garment_colors', function($t) {
			$t->dropForeign('garment_colors_garment_id_foreign');
		});

		Schema::drop('garment_colors');
		Schema::drop('sublimables');
		Schema::drop('garments');
		Schema::drop('garment_categories');
		Schema::drop('user_quotes');
		Schema::drop('sio_quotes');
		Schema::drop('sio_orders');
		
	
		
	}

}