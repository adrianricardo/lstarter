<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductCategories extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_categories', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->string('name');
			$table->integer('default_garment')->unsigned()->index();
			$table->foreign('default_garment')->references('id')->on('garments');
		});

		Schema::table('garments', function($table) {
			$table->integer('product_category_id')->nullable()->unsigned();
			$table->foreign('product_category_id')->references('id')->on('product_categories');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('garments', function($t) {
			$t->dropForeign('garments_product_category_id_foreign');
		});

		Schema::drop('product_categories');
	}

}
