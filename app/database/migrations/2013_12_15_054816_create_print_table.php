<?php

use Illuminate\Database\Migrations\Migration;

class CreatePrintTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('designs', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('vendor_designId')->nullable();
			$table->string('name');
			$table->string('type');
			$table->timestamps();
		});

		Schema::create('design_sides', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('design_id')->unsigned();
			$table->string('side');
			$table->string('artwork');
			$table->string('proof')->nullable();
			$table->string('colors')->nullable();
			$table->decimal('dimensions_width', 5, 2)->nullable();
			$table->decimal('dimensions_height', 5, 2)->nullable();
			$table->string('position_horizontal');
			$table->decimal('position_offset_top', 5, 2)->nullable();
			$table->decimal('position_offset_bottom', 5, 2)->nullable();
			$table->timestamps();
			$table->foreign('design_id')->references('id')->on('designs');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('design_sides');
		Schema::drop('designs');
	}

}