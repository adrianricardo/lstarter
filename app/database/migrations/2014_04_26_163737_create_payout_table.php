<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayoutTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function($t) {
        	$t->string('recipient_id')->nullable();
        });

		Schema::create('payouts', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('transfer_id');
			$table->integer('user_id')->unsigned();
			$table->decimal('amount', 6, 2);
			$table->string('recipient_id');
			$table->timestamps();
			$table->foreign('user_id')->references('id')->on('users');
		});

		Schema::table('order_items', function($t) {
        	$t->integer('payout_id')->unsigned()->nullable();
        	$t->foreign('payout_id')->references('id')->on('payouts');
        });

        Schema::table('sio_orders', function($t) {
        	$t->integer('payout_id')->unsigned()->nullable();
        	$t->foreign('payout_id')->references('id')->on('payouts');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sio_orders', function($t) {
			$t->dropForeign('sio_orders_payout_id_foreign');
        });
		Schema::table('order_items', function($t) {
			$t->dropForeign('order_items_payout_id_foreign');
        });
		Schema::drop('payouts');
		Schema::table('users', function($t) {
			$t->dropColumn('recipient_id');
        });
	}

}
