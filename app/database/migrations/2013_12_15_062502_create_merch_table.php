<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('garment_id')->unsigned();
			$table->integer('user_id');
			$table->integer('active')->default(0);
			$table->string('name')->nullable();
			$table->decimal('price', 6, 2)->nullable();
			$table->string('type')->default('shirt');
			$table->string('color');
			$table->integer('design_id')->nullable()->unsigned();
			$table->string('image')->nullable();
			$table->text('materials')->nullable();
			$table->text('description')->nullable();
			$table->integer('draft')->default(0);
			$table->timestamps();
			$table->foreign('design_id')->references('id')->on('designs');
		});

		Schema::table('order_items', function($t) {
			$t->foreign('product_id')->references('id')->on('products');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('order_items', function($t) {
			$t->dropForeign('order_items_product_id_foreign');
        });
		Schema::drop('products');
	}

}
