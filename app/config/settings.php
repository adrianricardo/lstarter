<?php

$key = (App::isLocal()) ? '7abc4866329ffdf03dcc0d0f39f96347' : '114baf60273b89fa6a69bcaee1d067c1';
return array(
    'shipping' => 4.99,
    'markup' => .98,
    'sp_api' => $key
);