<?php

class AppHelper {


	public static function adjustPrices($quote,$num_shirts = 1) {

		$response = array(
			'sio_subtotal'          => $quote->subtotal,
			'sio_total'             => $quote->total,
			'sio_price_per_shirt'   => $quote->total/$num_shirts,
			'murch_total'           => $quote->total+(Config::get('settings.markup')*$num_shirts),
			'murch_price_per_shirt' => ($quote->total/$num_shirts)+Config::get('settings.markup')
		);

		return $response;
	}
	

	/**
	 * @param  string  $filename
	 * @return string
	 */
	public static function asset_path($filename) {
		$manifest_path = 'compiled/'.$filename.'.json';

		if (file_exists($manifest_path)) {
			$manifest = json_decode(file_get_contents($manifest_path), TRUE);
		} else {
			$manifest = [];
		}

		if (array_key_exists($filename, $manifest)) {
			return $manifest[$filename];
		}

		return $filename;
	}


	public static function formatGarments($garments){
		$garment_id_list = $garments->lists('id');
		$garment_colors = GarmentColor::whereIn('garment_id',$garment_id_list)->get()->toArray();
		$garments = $garments->toArray();

		foreach ($garment_colors as $color) {
			$garment_color_ids[$color['garment_id']][] = $color['id'];
		}

		$i = 0;
		foreach ($garments as $garment) {
			if(!empty($garment['description_custom'])){
				$garments[$i]['description'] = $garments[$i]['description_custom'];
			}
			$garments[$i]['garment_colors'] = $garment_color_ids[$garment['id']];
			unset($garments[$i]['vendor_product_id']);
			unset($garments[$i]['description_custom']);
			if(!Auth::check()){
				$garments[$i]['base_price'] = 0;
			}

			$i++;
		}

		$result['garments'] = $garments;
		$result['garment_colors'] = $garment_colors;

		return $result;
	}


	public static function getGUID(){
		if (function_exists('com_create_guid')){
			return trim(com_create_guid(), '{}');
		}else{
			mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
			$charid = strtoupper(md5(uniqid(rand(), true)));
			$hyphen = chr(45);// "-"
			$uuid = ""
			.substr($charid, 0, 8).$hyphen
			.substr($charid, 8, 4).$hyphen
			.substr($charid,12, 4).$hyphen
			.substr($charid,16, 4).$hyphen
			.substr($charid,20,12);
			return $uuid;
		}
	}
}