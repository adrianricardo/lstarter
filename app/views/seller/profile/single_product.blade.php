@extends('profile.layouts.default')

{{-- Content --}}
@section('content')
	<div id="single-product-container" class="row">
		@if(empty($product->design_id))
			<div class="alert alert-info" role="alert">Product is missing a design. Please contact support</div>
		@else
			<div id="image-container" class="col-sm-6">
				@if(count($product->designSides))					
					@foreach ($product->designSides as $print)
						<img src="{{$print->proof}}" />
					@endforeach
				@else
					<img src="/assets/img/products/default.png" />
				@endif
			</div>
			<div id="product-info" class="col-sm-6">
				<h2 class="title">{{$product->name}}</h2>
				<p class="description">Printed on {{$product->garment->name}}</p>
				<p class="materials">{{$product->materials or $product->garment->materials}}</p>
				@if($currencies)
					<div id="converter-container">
						<select id="currency-select" data-price="{{$product->price}}">
							<option>Select currency</option>
							@foreach ($currencies as $currency)
								<option value="{{$currency->rate}}">{{$currency->code}}</option>
							@endforeach
						</select>
						<button type="button" id="converter-trigger">$ converter</button>
					</div>
				@endif
				<div class="price-container">
					<p class="price">${{$product->price}}</p>
					<p id="converted-price"></p>
					<p class="shipping">${{ Config::get('settings.shipping'); }} shipping<span>ships in 6-15 business days</span></p>
				</div>
				{{ Form::open(array('class'=>'form-horizontal','url' => '/checkout/add/'.$product->id)) }}
	 				<div class="form-group">
						<label for="quantity" class="col-xs-6 control-label">Quantity</label>
						<div class="col-xs-6">
					  		<select class="form-control" name="quantity" id="quantity">
					  			<option value="1" selected>1</option>
					  			<option value="2">2</option>
					  			<option value="3">3</option>
					  			<option value="4">4</option>
					  			<option value="5">5</option>
					  			<option value="6">6</option>
					  			<option value="7">7</option>
					  		</select>
					  		<input type="hidden" name="product_id" value="{{$product->id}}" />
						</div>
					</div>
					<div class="form-group">
						<label for="size" class="col-xs-6 control-label">Size</label>
						<div class="col-xs-6">
					  		<select class="form-control" name="size" id="size">
					  			<option value="sml" selected>Small</option>
					  			<option value="med">Medium</option>
					  			<option value="lrg">Large</option>
					  			<option value="xlg">XL</option>
					  		</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-6 col-xs-offset-6">
					  		<input type="submit" class="btn btn-primary btn-block" value="Buy" />
						</div>
					</div>
				</form>
				<a id="return-policy" href="/return-policy">Return/Exchange policy</a>
			</div>
		@endif
	</div>
@stop

@section('scripts')
	<script type="text/javascript">
		$(document).ready(function(){
			store.initCurrency();  
		})
	</script>
@endsection
@stop
