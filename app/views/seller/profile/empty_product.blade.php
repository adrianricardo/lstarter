@extends('profile.layouts.default')

{{-- Content --}}
@section('content')
	<div id="single-product-container">
		<div id="product-info" class="empty">
			<h2 class="title">No products</h2>
		</div>
	</div>
@stop
