@extends('profile.layouts.default')

{{-- Content --}}
@section('content')
	<ul id="product-container" class="{{count($products)}}">
	<?php 
		switch (count($products)) {
			case 1:
				$product_class = "col-xs-12";
				break;
			case 2:
				$product_class = "col-xs-6";
				break;
			default:
				$product_class = "col-xs-4";
				break;
		}
	?>
	@foreach ($products as $product)
		<li class={{$product_class}}>
			@foreach ($product->designSides as $print)
				<a href="/products/view/{{$product->id}}">
					<img src="{{$print->proof}}" />
				</a>
			@endforeach
				<a href="/products/view/{{$product->id}}">
					<h2 class="title">{{$product->name}}</h2>
				</a>
				<p class="price">${{$product->price}}</p>
				<?php /*<a href="/products/view/{{$product->id}}" class="btn">
					Buy now
				</a> */ ?>
		</li>
	@endforeach
	</ul>


@stop
