@extends('seller.layouts.default')

{{-- Web site Title --}}
@section('title')
Proucts :: @parent
@stop

{{-- Content --}}
@section('content')
<div class="col-md-12 clearfix" style="margin-bottom:100px">
	<h3>Balance</h3>
	<div class="col-md-8">
		<div id="balance">${{{$balance}}}</div>
		{{ Form::open(array('url' => 'dashboard/transfer-funds')) }}
		<input type="submit" class="btn btn-primary" value="Cash out" />
	</form>
</div>
<div class="col-md-4 bank-info">
	<p>Bank account (**********{{{$recipient->active_account->last4}}})</p>
	<a href="#">Update bank account</a>
		<?php /*
		<div class="alert alert-info">
			<h4>Bank account on file</h4>
			<strong>Bank name:</strong> {{{$recipient->active_account->bank_name}}}
			<br /><strong>Last 4:</strong> {{{$recipient->active_account->last4}}}
			<br /><strong>Country:</strong> {{{$recipient->active_account->country}}}  
			<br /><a href="#" style="margin-top:10px" class="pull-right">Update bank account</a>
		</div> */ ?>
	</div>
</div>

<table id="payouts" class="table table-striped table-hover">
	<thead>
		<tr>
			<th class="col-md-2">Payout ID</th>
			<th class="col-md-1">Amount</th>
			<th class="col-md-1">Date</th>
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>


@stop

{{-- Scripts --}}
@section('scripts')
<script type="text/javascript">
var oTable;
$(document).ready(function() {
	oTable = $('#payouts').dataTable( {
		"sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "_MENU_ records per page"
		},
		"bProcessing": true,
		"bServerSide": true,
		"bFilter": false,
		"sAjaxSource": "{{ URL::to('dashboard/payouts') }}",
		"fnDrawCallback": function ( oSettings ) {
			$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
		}
	});
	$('#payouts_wrapper').hide();
	$('#payouts').on('xhr.dt', function ( e, settings, json ) {
		if(json.iTotalRecords != 0){
			$('#payouts_wrapper').show();
		}
	});
});
</script>
@stop