@extends('seller.layouts.default')

{{-- Web site Title --}}
@section('title')
Proucts :: @parent
@stop

{{-- Content --}}
@section('content')
<div class="page-header">
	<h3>
		Get Paid
	</h3>
</div>
{{ Form::open(array('action' => array('SellerDashboardController@postCreateRecipient', Auth::user()->id), 'class' => 'form-horizontal','id' => 'bank-info')) }}
	<div class="form-group">
		<label class="col-md-2 control-label" for="name">Full legal name</label>
		<div class="col-md-10">
			<input class="form-control" type="text"  name="name" id="name" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-2 control-label" for="name">Account Number</label>
		<div class="col-md-10">
			<input class="form-control account-number" type="text" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-2 control-label" for="name">Routing Number</label>
		<div class="col-md-10">
			<input class="form-control routing-number" type="text" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-2 control-label" for="name">Country</label>
		<div class="col-md-10">
			<input class="form-control country" type="text" value="US" />
		</div>
	</div>

	<div class="form-group">
		<div class="col-md-10 col-md-offset-2">
			<button type="submit" class="btn btn-primary">Submit</button>
		</div>
	</div>
	<div class="bank-errors"></div>
</form>
@stop

{{-- Scripts --}}
@section('scripts')
	<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		main.initBankInfo('@stripeKey');  
	})
	</script>
@endsection