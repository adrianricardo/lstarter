@extends('seller.layouts.default')

{{-- Content --}}
@section('content')

@include('seller.products.application') 


@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
	main.initCreateProduct();  
})
</script>
<link href='/assets/css/animate.css' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:600' rel='stylesheet' type='text/css'>
<script src="/js/libs/handlebars-1.1.2.js"></script>
<script src="/js/libs/ember-1.7.0.js"></script>
<script src="/js/libs/ember-data.js"></script>
<script src="/js/app.js"></script>
<script src="/js/routes.js"></script>
<script src="/js/models.js"></script>
<script src="/js/routes.js"></script>
<script src="/js/controllers.js"></script>
<script src="/js/views.js"></script>
<script src="/assets/compiled/js/designer.js"></script>
@endsection
@stop