@extends('seller.layouts.default')

{{-- Web site Title --}}
@section('title')
	Proucts :: @parent
@stop

{{-- Content --}}
@section('content')
	<div class="page-header">
		<h3>
			Product Management
		</h3>
		<div class="header-buttons">
			<a href="{{{ URL::to('dashboard/products/create') }}}" class="btn btn-md btn-success"><span class="fa fa-plus"></span> Add new product</a>
			<?php /* <a href="{{{ URL::to('dashboard/products/create-new') }}}" class="" style="margin-top: 10px;display: inline-block;"><span class="fa fa-plus"></span> Add new product (BETA)</a> */ ?>
		</div>
	</div>
	@if($orderCount)
		<fieldset>
			<legend>Current batch totals</legend>
			<div class="stats">
				<div class="product-sold">
					{{$orderCount}}
					<span class="title"># Sold</span>
				</div>
				<div>
					${{$userQuote['murch_total']}}
					<span class="title">Total Cost</span>
				</div>
				<div class="product-cost">
					${{$userQuote['murch_price_per_shirt']}}
					<span class="title">Avg cost (per shirt)</span>
				</div>
				<div class="product-revenue">
					${{$revenueFromCurrentBatch - $userQuote['murch_total']}}
					<span class="title">Total Profit</span>
				</div>
				<div class="product-cost">
					<?php if($userQuote['murch_total'] > 0){ ?>
						${{($revenueFromCurrentBatch - $userQuote['murch_total'])/$orderCount}}
					<?php } ?>
					
					<span class="title">Avg profit (per shirt)</span>
				</div>
				<div class="product-profit">
					
				</div>
			</div>
		</fieldset>
	@endif
	<ul class="products">
		@if(!count($products))
			<h4>You haven't created any products</h4>
		@endif
		@foreach ($products as $product)
			<?php $productImage = "/assets/img/products/default.png"; /*
			@foreach ($product->printSides as $side)
				<?php 
					$productImage = $side->proof;
					if($side->side == "front") break;
				?>
			@endforeach */ ?>
			<li class="<?php if(!$product->active) echo "not-active"; ?>">
				<img src="{{$productImage}}" />
				<h3>{{$product->name}} <?php if($product->active) echo link_to('products/view/'.$product->id,'View',$attributes = array('class'=>'view-link')); ?> </h3>
				<fieldset>
					<legend>Current batch</legend>
					<div class="stats">
						<div class="product-sold">
							<?php $qty = 0?>
							@foreach ($product->orderItems as $item)
								@if(is_null($item->sio_order_id))
									<?php $qty += $item->quantity;?>
								@endif
							@endforeach
							{{$qty}}
							<span class="title"># Sold</span>
						</div>
						<div class="product-cost">
							${{{$productQuotes[$product->id]['murch_price_per_shirt'] or "0"}}}
							<span class="title">Print cost (per shirt)</span>
						</div>
						<div class="product-revenue">
							<?php $revenue = 0; ?>
							@foreach ($product->orderItems as $item)
								@if(is_null($item->sio_order_id))
									<?php $revenue += $item->quantity*$item->price_at_sale;?>
								@endif
							@endforeach
							${{{$revenue}}}
							<span class="title">Revenue</span>
						</div>
						<div class="product-profit">
							@if(isset($productQuotes[$product->id]['murch_total']) && $revenue-$productQuotes[$product->id]['murch_total'] > 0)
								${{{$revenue-($productQuotes[$product->id]['murch_total'])}}}
							@else
								$0
							@endif
							<span class="title">Estimated profits</span>
						</div>
					</div>
				</fieldset>
				<?php $qty = 0?>
				@foreach ($product->orderItems as $item)
					@if(!is_null($item->sio_order_id))
						<?php $qty += $item->quantity;?>
					@endif
				@endforeach
				@if($qty > 0)
					<fieldset>
						<legend>Past batches</legend>
						<div class="stats">
							<div class="product-sold">
								{{$qty}}
								<span class="title"># Sold</span>
							</div>
							<?php /*
							<div class="product-revenue">
								<?php $revenue = 0; ?>
								@foreach ($product->orderItems as $item)
									@if(!is_null($item->sio_order_id))
										<?php $revenue += $item->quantity*$item->price_at_sale;?>
									@endif
								@endforeach
								${{{$revenue}}}
								<span class="title">Revenue</span>
							</div> 
							<div class="product-payouts">
								${{{$payouts[$product->id]->amount or '0'}}}
								<span class="title">Paid out</span>
							</div>
							*/ ?>
							@if($balance)
								<div class="product-getpaid">
									${{{$balance}}}
									<span class="title">Pending payout</span>
								</div>
								{{ link_to_route('get-paid', 'Cash out', null, $attributes = array('class'=>'btn btn-success'));}}
							@endif
						</div>
					</fieldset>
				@endif
			</li>
		@endforeach
	</ul>
@stop

{{-- Scripts --}}
@section('scripts')

@stop