<script type="text/x-handlebars" data-template-name="application">
	{{outlet}}
</script>

<?php /*
<script type="text/x-handlebars" data-template-name="index">
	<h4 style="margin-bottom:40px">Select a category below to get started</h4>
	<div id="product-selector">
		{{#each model}}
			{{#link-to 'garment' default_garment class="col-md-4"}}
				<img src="/assets/img/create/{{unbound name}}.png" />
				<div class="category-name">{{unbound name}}</div>
			{{/link-to}}
		{{/each}}
	</div>
</script> */ 
?>

<script type="text/x-handlebars" data-template-name="index">
	<div class="page-header">
		<h3>
			Product Management
		</h3>
		<div class="header-buttons">
			{{#link-to 'new' class="btn btn-md btn-success"}}<span class="fa fa-plus"></span> Add new product{{/link-to}}
		</div>
	</div>
	<ul class="products">

	</ul>
</script>



<script type="text/x-handlebars" data-template-name="change-product">
	<ul id="product-selector">
		<li {{action 'setCategory' 1 }} class="col-md-4">
			<img src="/assets/img/create/shirt.png" />
			<div class="category-name">shirt</div>
		</li>
		<li {{action 'setCategory' 2 }} class="col-md-4">
			<img src="/assets/img/create/sweatshirt.png" />
			<div class="category-name">sweatshirt</div>
		</li>
		<li {{action 'setCategory' 3 }} class="col-md-4">
			<img src="/assets/img/create/hoodie.png" />
			<div class="category-name">hoodie</div>
		</li>
	</ul>
	<ul id="garment-list">
		{{#each filteredContent}}
			<li {{action 'setProduct' id}}>
					<div class="image-container">
						<img {{bind-attr src=garment_colors.firstObject.front_image}} /> 
					</div>
					<div class="garment-info">
						<div class="garment-name">{{unbound name}}</div>
						<div class="garment-brand">{{unbound brand}}</div>
						<div class="product-price"><span>STARTS AT</span> {{unbound base_price_helper base_price}}</div>
						<p class="garment-description">{{unbound description}}</p>
						<p class="garment-materials">{{unbound materials}}</p>
						<div id="color-list" class="btn-group color-list" data-toggle="buttons">
							{{#each garment_colors}}
								<button data-name="{{unbound name}}" style="background-color:{{unbound color}}"></button>
							{{/each}}
						</div>
					</div>
			</li>
		{{/each}}
	</ul>
</script>

<script type="text/x-handlebars" data-template-name="designer">
	<div id="designer-tool">
		<?php /*<div id="garment-tip">
			Try the <span class="brand">"Next Level CVC Crew"</span>. $4 cheaper than American Apparel but still a premium quality and fit.
		</div> */ ?>
		<div id="product-preview">
			<img id="front_image" {{bind-attr src=App.productPreview.front_image}} />
		</div>
		<div id="product-details">
			{{outlet}}
		</div>
	</div>
</script>


<script type="text/x-handlebars" data-template-name="new">
	<h3 id="product-type">{{#link-to 'change-product' }}13 other styles{{/link-to}}<span class="fa fa-arrow-circle-o-right"></span></h3>
	<h4>Style</h4>
	<p class="garment-name">{{name}}</p>
	<p class="garment-brand">{{brand}}</p>
	<p class="product-price">{{base_price_helper App.productPreview.quote}}</p>
	<p class="garment-description">{{description}}</p>
	<p class="garment-materials">{{materials}}</p>

	<h4>Color</h4>		
	<div id="color-list" class="btn-group color-list" data-toggle="buttons">
		{{#each garment_colors}}
			<button class="has-tooltip" {{action 'setColor' name front_image}} data-name="{{unbound name}}" style="background-color:{{unbound color}}" data-toggle="tooltip" data-placement="bottom" title="{{unbound name}}"></button>
		{{/each}}
	</div>
	{{#if App.user_id}}
		{{#if App.newProduct.color }}
			<button class="btn-green btn btn-block" {{action 'saveProduct'}}>Next</button>
		{{else}}
			<div class="btn-green btn btn-block disabled">Choose a color</div>
		{{/if}}
	{{else}}
		<a target="_blank" class="btn-green btn btn-block" href="/login">Next</a>
	{{/if}}
</script>

<script type="text/x-handlebars" data-template-name="product-details">
	<p class="product-price">{{base_price_helper quote}}</p>

	<form class="form-horizontal">
		<div class="form-group">
			<label class="col-md-2 control-label" for="name">Name:</label>
			<div class="col-md-10">
				{{view Ember.TextField valueBinding="App.newProduct.name" class="form-control"}}
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label" for="price">Price:</label>
			<div class="col-md-4 col-md-offset-6">
				<div class="input-group">
					<span class="input-group-addon">$</span>
					{{view Ember.TextField valueBinding="App.newProduct.price" class="form-control"}}
				</div>
			</div>
		</div>

		<div class="form-group print-sides">
			<label class="col-md-2 control-label" for="price">Sides</label>
			<div class="col-md-10">
				<div class="btn-group-vertical btn-block" data-toggle="buttons">
					<label class="btn btn-primary side active" {{action 'updateSides'}}>
						<input type="checkbox"> Front print
					</label>
					<label class="btn btn-primary side" {{action 'updateSides'}}>
						<input type="checkbox"> Back print
					</label>
				</div>
			</div>
		</div>

		
	</form>

	{{#if App.newProduct.name }}
		{{#if App.newProduct.price }}
			<button class="btn-green btn btn-block" {{action 'saveProduct'}}>Save</button>
		{{else}}
			<div class="btn-green btn btn-block disabled">Enter a selling price</div>
		{{/if}}
	{{else}}
		<div class="btn-green btn btn-block disabled">Enter a name for this product</div>
	{{/if}}
</script>

<script type="text/x-handlebars" data-template-name="thank-you">
	<h4>Thank you!</h4>
	<p>Your selection has been saved. We wil contact you shortly with the next steps</p>
	<a href="/dashboard/products/create-new" class="btn btn-green btn-block">Add another</a>
	<a href="/" class="btn btn-green btn-block">Go to dashboard</a>
</script>

<div id="design-tool-container">
</div>