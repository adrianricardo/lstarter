@extends('seller.layouts.default')

{{-- Content --}}
@section('content')

	<div class="page-header">
		<h3>
			New Product
		</h3>
	</div>


	{{-- Create Merch Form --}}		
	{{ Form::open(array('class' => 'form-horizontal','id' => 'create-product','files'=> true)) }}
		<div class="form-group {{{ $errors->has('garment_product_id') ? 'error' : '' }}}">
			<label class="col-md-2 control-label" for="name">Shirt type</label>
			<div class="col-md-10">
				<div class="btn-group" data-toggle="buttons">
					@foreach ($products as $product)		
					  		<label class="btn btn-primary product-option" data-id="{{$product->vendor_product_id}}">
					  			<img src="/assets/img/products/default.png" class="product-image" />
					    		<input type="radio" name="garment_product_id" id="product{{$product->vendor_product_id}}" value="{{$product->vendor_product_id}}"> 
					    		{{$product->name}}
					    		<span class="comment">{{$product->comments}}</span>
					  		</label>
					@endforeach
				</div>
				{{{ $errors->first('garment_product_id', '<span class="help-inline">:message</span>') }}}
			</div>
		</div>


		<div class="form-group {{{ $errors->has('color') ? 'error' : '' }}}">
			<label class="col-md-2 control-label" for="name">Color</label>
			<div class="col-md-10">
					@foreach ($colors as $key=>$value)
						<div id="color-{{$key}}" class="btn-group color-list" data-toggle="buttons">		
 							@foreach ($value as $color) 
 								<label class="btn" style="background-color:{{$color->color}}">
						    		<input type="radio" name="color" value="{{$color->name}}"> 
						  		</label>
 							@endforeach
 						</div>
 					@endforeach
				{{{ $errors->first('color', '<span class="help-inline">:message</span>') }}}
			</div>
		</div>
		<div class="form-group">
			<div id="sample-product" class="sample-product col-md-4 col-md-offset-2">
				<img src="/assets/img/products/shirt-template.png" />
				<div class="print-area"></div>
				<button class="btn btn-primary " data-toggle="modal" data-target="#front-artwork-modal">
				  Upload image
				</button>
				<div id="front-color-count" class="color-count" style="display:none">
					<span class="glyphicon glyphicon-file"></span>
					<span id="front-filename" class="filename"></span>
				</div>
			</div>
			<div class="sample-product col-md-4">
				<img src="/assets/img/products/shirt-template.png" />
				<div class="print-area"></div>
				<button class="btn btn-primary " data-toggle="modal" data-target="#back-artwork-modal">
				  Upload image
				</button>
			</div>
		</div>


		<div class="form-group {{{ $errors->has('name') ? 'error' : '' }}}">
			<label class="col-md-2 control-label" for="name">Name</label>
            <div class="col-md-10">
				<input class="form-control" type="text" name="name" id="name" value="{{{ Input::old('name') }}}" />
				{{{ $errors->first('name', '<span class="help-inline">:message</span>') }}}
            </div>
		</div>
		<!-- ./ color -->
		<div class="form-group {{{ $errors->has('price') ? 'error' : '' }}}">
			<label class="col-md-2 control-label" for="name">Price</label>
            <div class="col-md-10">
				<input class="form-control" type="number" name="price" id="price" value="{{{ Input::old('price') }}}" min="0" step="any"/>
				{{{ $errors->first('price', '<span class="help-inline">:message</span>') }}}
            </div>
		</div>
		<!-- ./ print_type -->

		<!-- Form Actions -->
		<div class="form-group">
            <div class="col-md-offset-2 col-md-10">
            	<input type="hidden" name="type" value="shirt" />
            	<input type="hidden" name="photo" id="photo" />
				<element class="btn-cancel close_popup">Cancel</element>
				<button type="reset" class="btn btn-default">Reset</button>
				<button type="submit" class="btn btn-success">Create</button>
            </div>
		</div>
		<!-- ./ form actions -->

		<!-- Modal -->
		<div class="modal fade artwork-modal" id="front-artwork-modal" tabindex="-1" role="dialog" aria-hidden="true" data-side="front">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        <h4 class="modal-title" id="myModalLabel">Front - Artwork</h4>
		      </div>
		      <div class="modal-body">
		      	<div class="alert alert-info">For best results and prices, upload high resolution/vector art.</div>
		      	<input class="form-control" type="file" name="front-artwork" id="front-artwork" />
		      	<p class="help-block">File types: eps, png</p>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-primary" data-dismiss="modal">Continue</button>
		      </div>
		    </div><!-- /.modal-content -->
		  </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<!-- Modal -->
		<div class="modal fade" id="back-artwork-modal" tabindex="-1" role="dialog" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        <h4 class="modal-title" id="myModalLabel">Back - Artwork</h4>
		      </div>
		      <div class="modal-body">
		      	<input class="form-control" type="file" name="back-artwork" />
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Continue</button>
		      </div>
		    </div><!-- /.modal-content -->
		  </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

	{{ Form::close() }}
	@section('scripts')
    	<script type="text/javascript">
			$(document).ready(function(){
	     		main.initCreateProduct();  
	    	})
		</script>
	@endsection
@stop
