@extends('seller.layouts.default')

{{-- Web site Title --}}
@section('title')
	Proucts :: @parent
@stop

{{-- Content --}}
@section('content')
	<div class="page-header">
		<h3>
			My products

			<div class="pull-right">
				<a href="{{{ URL::to('dashboard/products/create') }}}" class="btn btn-small btn-info"><span class="fa fa-plus"></span> Create</a>
			</div>
		</h3>
	</div>
<table id="products" class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-2">Name</th>
				<th class="col-md-1">Item count</th>
				<th class="col-md-1">Total</th>
				<th class="col-md-1">State</th>
				<th class="col-md-1">Date</th>
		</thead>
		<tbody>
		</tbody>
	</table>
@stop

{{-- Scripts --}}
@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {
				oTable = $('#products').dataTable( {
				"sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
				"sPaginationType": "bootstrap",
				"oLanguage": {
					"sLengthMenu": "_MENU_ records per page"
				},
				"bProcessing": true,
		        "bServerSide": true,
		        "sAjaxSource": "{{ URL::to('dashboard/orders/data') }}",
		        "fnDrawCallback": function ( oSettings ) {
	           		$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	     		}
			});
		});
	</script>
@stop