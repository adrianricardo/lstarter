<!DOCTYPE html>
<html lang="en">
	<head>
		<script type="text/javascript">
		  WebFontConfig = {
			google: { families: [ 'Lato:300:latin', 'Pacifico::latin', 'Oswald::latin' ] }
		  };
		  (function() {
			var wf = document.createElement('script');
			wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
			  '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
			wf.type = 'text/javascript';
			wf.async = 'true';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(wf, s);
		  })(); 
		</script>

		<!-- Basic Page Needs
		================================================== -->
		<meta charset="utf-8" />
		<title>
			@section('title')
			Murch.co - Print,Ship, and Sell merchandise
			@show
		</title>
		<meta name="keywords" content="Artist, merch, merchandise, print, ship, sell" />
		<meta name="author" content="Murch.co" />

		<meta name="description" content="The easiest way to print, ship, and sell merch to your fans." />
		<meta property="og:title" content="The easiest way to print, ship, and sell merch to your fans." />
		<meta property="og:image" content="http://murch.co/assets/img/fb_logo.jpg"/

		<!-- Mobile Specific Metas
		================================================== -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- CSS
		================================================== -->
		<link rel="stylesheet" href="compiled/public/css/{{ AppHelper::asset_path('public.css') }}">

		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

		<style>
		@section('styles')
		@show
		</style>

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->


		<!-- Favicons
		================================================== -->
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{{ asset('assets/ico/apple-touch-icon-144-precomposed.png') }}}">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{{ asset('assets/ico/apple-touch-icon-114-precomposed.png') }}}">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{{ asset('assets/ico/apple-touch-icon-72-precomposed.png') }}}">
		<link rel="apple-touch-icon-precomposed" href="{{{ asset('assets/ico/apple-touch-icon-57-precomposed.png') }}}">
		<link rel="shortcut icon" href="{{{ asset('assets/ico/favicon.png') }}}">

		<script src="compiled/public/js/{{ AppHelper::asset_path('public.js') }}"></script>

		<!-- Javascripts
		================================================== -->
	</head>

	<body class="landing">
		<!-- To make sticky footer need to wrap in a div -->
		<div id="wrap">		

		<!-- Container -->
		<div class="container">
			<!-- Notifications -->
			@include('notifications')
			<!-- ./ notifications -->

			<!-- Content -->
			@yield('content')
			<!-- ./ content -->
		</div>
		<!-- ./ container -->

		<!-- the following div is needed to make a sticky footer -->
		
		</div>
		<!-- ./wrap -->
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-49467501-2', 'murch.co');
		  ga('send', 'pageview');

		</script>
		@yield('scripts')
	</body>
</html>
