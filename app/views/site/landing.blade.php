@extends('site.layouts.landing')

@section('scripts')
<link href='/assets/css/animate.css' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:600' rel='stylesheet' type='text/css'>
<script src="/js/libs/handlebars-1.1.2.js"></script>
<script src="/js/libs/ember-1.7.0.js"></script>
<script src="/js/libs/ember-data.js"></script>
<script src="/js/app.js"></script>
<script src="/js/routes.js"></script>
<script src="/js/models.js"></script>
<script src="/js/routes.js"></script>
<script src="/js/controllers.js"></script>
<script src="/js/views.js"></script>
<script src="/assets/compiled/js/designer.js"></script>
<script>
	App.IndexRoute = Ember.Route.extend({
		redirect: function() {
			this.transitionTo('new'); 
		}
	});

	App.Router.reopen({
		location: 'none'
	});
</script>
@endsection


{{-- Content --}}
@section('content')
<div class="row" id="main-landing">
	<div id="left-col">
		<h1>{{ HTML::image('/assets/img/logo.png', 'Murch Collective')}}</h1>
	</div>
	<div id="right-col">
		<h3>Let us <span class="accent">sell</span> , <span class="accent">print</span> , and <span class="accent">ship </span>  your merchandise directly to your fans.</h3>

		{{ Form::open(array('class' => 'form-inline')) }}
			<div class="form-group">
				<input class="form-control" type="email" name="email" placeholder="you@example.com" required />
			</div>
			<button type="submit" class="btn btn-ghost btn-white">Signup</button>
		{{ Form::close() }}
	</div>
</div>
<div class="row" style="margin-top:30px">
	<a href="/signup" class="btn btn-default btn-large">Signup</a> 
	<a href="/login" class="btn btn-default btn-large">Login</a> 
</div>
<div class="row" id="design-tool-container">
	<h2>TRY IT OUT</h2>
	@include('seller.products.application') 
</div>
<div class="row" id="how-it-works">
	<h2>HOW IT WORKS</h2>
	<ul>
		<li>
			<img src="/assets/img/landing/price.jpg" />
			<p>Upload your design and set your selling price</p>
		</li>
		<li>
			<img src="/assets/img/landing/store.jpg" />
			<p>Create and share your instant online store</p>
		</li>
		<li>
			<img src="/assets/img/landing/truck.jpg" />
			<p>We print and ship directly to customers</p>
		</li>
		<li>
			<img src="/assets/img/landing/dollar.jpg" />
			<p>Cash out profits to your bank account!</p>
		</li>
	</ul>
	<a href="/signup" type="button" class="btn btn-ghost">Get Started</button>
</div>
<?php /* <div class="row" id="pricing">
	<h2>PRICING</h2>
	<section class="price">
		<span class="price-sign">$</span>
		<span class="price-number">5</span>
		<span class="price-frequency"><span>/</span>month</span>
	</section>
	<section class="price-description">
		<p>$5 is all you need to open an <b>inventory-free</b> merchandise store. Never pay us or anyone else a dime more.</p>
		<p>Cancel anytime.</p>
	</section>
</div> */ ?>
<a id="mail-link" href="mailto:info.murch.co@gmail.com" target="_blank">Contact <span class="fa fa-envelope"></span></a>
<!-- Modal -->
<div class="modal fade" id="beta-signup" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel">Murch.co is currently invite only</h4>
			</div>
			<div class="modal-body">
				<p>Leave us some info below and we will let you know as soon as we have a store ready for you!</p> 
				{{ Form::open(array('class' => 'form-horizontal')) }}
					<div class="input-group">
						<input class="form-control" type="email" name="email" placeholder="you@example.com" required />
					</div>
					<div class="input-group">
						<input class="form-control" type="text" name="twitter" placeholder="twitter handle" />
					</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Signup</button>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>


@stop
