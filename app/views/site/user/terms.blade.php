@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
Terms of Service
@parent
@stop

{{-- Content --}}
@section('content')
<div class="page-header">
    <h1>Terms of service</h1>
</div>

<div class="terms-of-service">
	<h5>Copyrights</h5>
	<p>Seller must hold all necessary copyrights for any artwork sold through Murch.co</p>
	<h5>Samples</h5>
	<p>Seller has option to test quality of print and garments by purchasing a sample print.</p>
	<h5>Cancellations</h5>
	<p>Once an order has been submitted to be printed, it can not be canceled.</p>
	<h4>Artwork</h4>
	<h5>Placement/Size</h5>
	<p>Seller must explicitly choose the placement and size of the artwork. For example: "9.5 inches wide artwork placed 4 inches below the collar". If Murch.co staff recommends a size/placement combination, the seller must accept the recommendation or suggest new size and placement values.</p>
	<h5>Refund</h5>
	<p>Refunds are not allowed</p>
	<h5>Print issues</h5>
	<p>If a print is more than 1 inch off from the requested size/placement, the individual print can be mailed back and refunded. The seller or seller's customer is responsible for paying the shipping required to mail the print back.</p>
	<h4>Getting paid</h4>
	<p>Seller must provide a scan of their drivers license to prove their identity</p>
</div>
@stop
