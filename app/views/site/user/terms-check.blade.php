@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
Terms of Service
@parent
@stop

{{-- Content --}}
@section('content')
<div class="page-header">
    <h1>Terms of service</h1>
</div>

<div class="terms-of-service">
	<p>Please check the box below to confirm you have read and agreed to the <a href="/terms-of-service" target="_blank" style="text-decoration:underline">terms of service.</a></p>
	<div class="panel panel-info" style="padding:20px">
		{{ Form::open(array('url' => '/user/terms-check')) }}
			 <div class="form-group">
				<label>
					{{ Form::checkbox('agreed_to_terms','1',false)}} I have read and agreed to the terms of service
				</label>
			</div>
			<div class="form-group">
				{{ Form::submit('I agree',array('class' => 'btn btn-primary'));}}
			</div>
		{{ Form::close() }}
	</div>
</div>
@stop
