@extends($layout)

{{-- Web site Title --}}
@section('title')
{{{ Lang::get('user/user.settings') }}} ::
@parent
@stop


{{-- Content --}}
@section('content')
<div class="page-header">
	<h3>Edit your settings</h3>
</div>
	{{ Form::open(array('class' => 'form-horizontal','files'=> true,'url'=>URL::to('user/' . $user->id . '/edit'))) }}
	<!-- CSRF Token -->
	<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
	<!-- ./ csrf token -->
	<!-- General tab -->
	<div class="tab-pane active" id="tab-general">
		<!-- username -->
		<div class="form-group {{{ $errors->has('username') ? 'error' : '' }}}">
			<label class="col-md-2 control-label" for="username">Username</label>
			<div class="col-md-10">
				<input class="form-control" type="text" name="username" id="username" value="{{{ Input::old('username', $user->username) }}}" />
				{{ $errors->first('username', '<span class="help-inline">:message</span>') }}
			</div>
		</div>
		<!-- ./ username -->

		<!-- Email -->
		<div class="form-group {{{ $errors->has('email') ? 'error' : '' }}}">
			<label class="col-md-2 control-label" for="email">Email</label>
			<div class="col-md-10">
				<input class="form-control" type="text" name="email" id="email" value="{{{ Input::old('email', $user->email) }}}" />
				{{ $errors->first('email', '<span class="help-inline">:message</span>') }}
			</div>
		</div>
		<!-- ./ email -->

		<!-- display_name -->
		<div class="form-group {{{ $errors->has('display_name') ? 'error' : '' }}}">
			<label class="col-md-2 control-label" for="display_name">Display name</label>
			<div class="col-md-10">
				<input class="form-control" type="text" name="display_name" id="display_name" value="{{{ Input::old('display_name', $user->display_name) }}}" />
				{{{ $errors->first('display_name', '<span class="help-inline">:message</span>') }}}
			</div>
		</div>
		<!-- ./ username -->

		<!-- Password -->
		<div class="form-group {{{ $errors->has('password') ? 'error' : '' }}}">
			<label class="col-md-2 control-label" for="password">Password</label>
			<div class="col-md-10">
				<input class="form-control" type="password" name="password" id="password" value="" />
				{{ $errors->first('password', '<span class="help-inline">:message</span>') }}
			</div>
		</div>
		<!-- ./ password -->

		<!-- Password Confirm -->
		<div class="form-group {{{ $errors->has('password_confirmation') ? 'error' : '' }}}">
			<label class="col-md-2 control-label" for="password_confirmation">Password Confirm</label>
			<div class="col-md-10">
				<input class="form-control" type="password" name="password_confirmation" id="password_confirmation" value="" />
				{{ $errors->first('password_confirmation', '<span class="help-inline">:message</span>') }}
			</div>
		</div>
		<!-- ./ password confirm -->

		<!-- Password Confirm -->
		<div class="form-group {{{ $errors->has('profile_image') ? 'error' : '' }}}">
			<label class="col-md-2 control-label" for="profile_image">Profile image</label>
			<div class="col-md-10">
				<input class="form-control" type="file" name="profile_image" id="profile_image" value="" />
				{{{ $errors->first('profile_image', '<span class="help-inline">:message</span>') }}}
			</div>
		</div>
		<!-- ./ password confirm -->

		<fieldset>
			<legend>Social</legend>
			
			<div class="form-group {{{ $errors->has('facebook') ? 'error' : '' }}}">
				<label class="col-md-2 control-label" for="facebook">Facebook</label>
				<div class="col-md-10">
					<input class="form-control" type="text" name="facebook" id="facebook" value="{{{ Input::old('facebook', $user->facebook) }}}" />
					{{{ $errors->first('facebook', '<span class="help-inline">:message</span>') }}}
				</div>
			</div>

			<div class="form-group {{{ $errors->has('twitter') ? 'error' : '' }}}">
				<label class="col-md-2 control-label" for="twitter">Twitter</label>
				<div class="col-md-10">
					<input class="form-control" type="text" name="twitter" id="twitter" value="{{{ Input::old('twitter', $user->twitter) }}}" />
					{{{ $errors->first('twitter', '<span class="help-inline">:message</span>') }}}
				</div>
			</div>

			<div class="form-group {{{ $errors->has('instagram') ? 'error' : '' }}}">
				<label class="col-md-2 control-label" for="instagram">Instagram</label>
				<div class="col-md-10">
					<input class="form-control" type="text" name="instagram" id="instagram" value="{{{ Input::old('instagram', $user->instagram) }}}" />
					{{{ $errors->first('instagram', '<span class="help-inline">:message</span>') }}}
				</div>
			</div>
			
		</fieldset>

	</div>
	<!-- ./ general tab -->

	<!-- Form Actions -->
	<div class="form-group">
		<div class="col-md-offset-2 col-md-10">
			<button type="submit" class="btn btn-success">Update</button>
		</div>
	</div>
	<!-- ./ form actions -->
</form>
</form>
@stop
