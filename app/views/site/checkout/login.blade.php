@extends($layout)

{{-- Web site Title --}}
@section('title')
{{{ Lang::get('user/user.login') }}} ::
@parent
@stop

{{-- Content --}}
@section('content')
<div class="col-sm-6">
    <div class="page-header">
    	<h1>Login into your account</h1>
    </div>
    <form class="form-horizontal" method="POST" action="{{ URL::to('user/login') }}" accept-charset="UTF-8">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label" for="email">{{ Lang::get('confide::confide.username_e_mail') }}</label>
                <div class="col-md-10">
                    <input class="form-control" tabindex="1" placeholder="{{ Lang::get('confide::confide.username_e_mail') }}" type="text" name="email" id="email" value="{{ Input::old('email') }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label" for="password">
                    {{ Lang::get('confide::confide.password') }}
                </label>
                <div class="col-md-10">
                    <input class="form-control" tabindex="2" placeholder="{{ Lang::get('confide::confide.password') }}" type="password" name="password" id="password">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <button tabindex="3" type="submit" class="btn btn-primary">{{ Lang::get('confide::confide.login.submit') }}</button>
                    <a class="btn btn-default" href="forgot">{{ Lang::get('confide::confide.login.forgot_password') }}</a>
                </div>
            </div>
        </fieldset>
    </form>
</div>
<div class="col-sm-6">
    <div class="page-header">
        <h1>Continue without logging in</h1>
    </div>
    <a href="/checkout/cart" class="btn btn-default btn-lg" >Continue as guest</a>
</div>

<script>
    $(document).ready(function(){
      main.initPurchaseAuth()  
    })
</script>

@stop
