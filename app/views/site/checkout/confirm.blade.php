@extends('profile.layouts.default')

{{-- Content --}}
@section('content')
	<div class="page-header">
		<h3>Confirm order</h3>
	</div>
	<div clas="row">
		<div class="sidebar col-md-3">
			{{ Form::open(array('class' => 'form-horizontal')) }}
				<div>
					<h2 style="margin-top:0">Deliver to: <a class="cart-change" href="/checkout/">(Change)</a></h2>
					<p>{{$data['stripeShippingName']}}
					<br />{{$data['stripeShippingAddressLine1']}}
					<br />{{$data['stripeShippingAddressCity']}}, {{$data['stripeShippingAddressState'] or ""}} {{$data['stripeShippingAddressZip']}}
					<br />{{$data['stripeShippingAddressCountry']}}</p>
				</div>
				<span class="shipping">U.S. orders delivered in 6-15 business days</span>
				<div id="submit-row" class="text-right">
					<button type="submit" class="btn btn-primary btn-lg btn-block">Place Order<i class="fa fa-arrow-circle-right"></i></button>
				</div>
				</div>
			{{ Form::close() }}
		</div>
		{{$cart_view}}
	</div>


@stop
