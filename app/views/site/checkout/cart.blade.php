@extends($layout)

{{-- Content --}}
@section('content')
	<div class="page-header">
		<h3>Cart</h3>
	</div>
	@if($cart)
		{{$cart_view}}
		{{ Form::open(array('class'=>'form-horizontal','url' => '/checkout/process/')) }}
			<div class="form-group">
				<div class="col-sm-12" style="text-align:right">
					<script 
						src="https://checkout.stripe.com/checkout.js" class="stripe-button"
						data-key="@stripeKey"
						data-name="{{Config::get('app.sitename')}}"
						data-description="{{Config::get('app.sitename')}} Order"
						data-image="/assets/img/logo-128x128.jpg"
						data-label="Submit order"
						data-shipping-address="true"
						data-billing-address="true">
					</script>
				</div>
			</div>
		</form>
	@else
		<h3>Your cart is empty.</h3>
	@endif



@stop
