@extends('site.layouts.default')

{{-- Content --}}
@section('content')
	<div class="page-header">
		<h3>Shipping Address</h3>
	</div>
	 @if ( $errors->count() > 0 )
      <div class="alert alert-warning">
        @foreach( $errors->all() as $message )
          <li>{{ $message }}</li>
        @endforeach
      </div>
    @endif
	{{ Form::open(array('class' => 'form-horizontal')) }}
    	<div class="form-group">
			<label for="name" class="col-sm-2 control-label">Full name</label>
			<div class="col-sm-10">
		  		<input type="text" class="form-control" name="name" id="name" placeholder="Full name">
		  		{{{ $errors->has('name', '<span class="help-inline">:message</span>') }}}
			</div>
		</div>
		<div class="form-group">
			<label for="street1" class="col-sm-2 control-label">Street 1</label>
			<div class="col-sm-10">
		  		<input type="text" class="form-control" name="street1" id="street1" placeholder="315 Main st">
			</div>
		</div>
		<div class="form-group">
			<label for="street2" class="col-sm-2 control-label">Street 2</label>
			<div class="col-sm-10">
		  		<input type="text" class="form-control" name="street2" id="street2" placeholder="Apt 905">
			</div>
		</div>
		<div class="form-group">
			<label for="city" class="col-sm-2 control-label">City</label>
			<div class="col-sm-10">
		  		<input type="text" class="form-control" name="city" id="city" placeholder="Austin">
			</div>
		</div>
		<div class="form-group">
			<label for="state" class="col-sm-2 control-label">State</label>
			<div class="col-sm-10">
		  		<input type="text" class="form-control" name="state" id="state" placeholder="Texas">
			</div>
		</div>
		<div class="form-group">
			<label for="Zip" class="col-sm-2 control-label">Zip</label>
			<div class="col-sm-10">
		  		<input type="text" class="form-control" name="zip" id="zip" placeholder="78705">
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-10 col-sm-offset-2">
		  		<input type="submit" class="btn btn-primary">
			</div>
		</div>
	{{ Form::close() }}


@stop
