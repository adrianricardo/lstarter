@extends($layout)

{{-- Content --}}
@section('content')
	<div class="page-header">
		<h3>Thank you for your order! Order #{{$order->id}}</h3>
	</div>
	<p>U.S. delivery takes 6-15 business days</p>
	<table class="table">
		<thead>
			<tr>
				<th>Item</th>
				<th>Quantity</th>
				<th>Price</th>
				<th>Total</th>
			</tr>
		</thead>
		<tbody>
			@foreach($order_items as $item)
			<tr>
				<td>{{$item->name}}</td>
				<td>{{$item->quantity}}</td>
				<td>${{$item->price}}</td>
				<td>${{$item->price*$item->quantity}}</td>
			</tr>
			@endforeach
		</tbody>
		<tfoot>
			<tr>
				<td></td>
				<td></td>
				@if($order->shipping_country == "US")
					<td>Shipping:</td>
				@else
					<td>International shipping:</td>
				@endif
				<td>${{$order->shipping}}</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>Total:</td>
				<td><b>${{$order->total}}</b></td>
			</tr>
		</tfoot>
	</table>

@stop
