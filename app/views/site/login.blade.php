@extends('site.layouts.landing')

@section('scripts')
<link href='/assets/css/animate.css' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:600' rel='stylesheet' type='text/css'>
@endsection


{{-- Content --}}
@section('content')
<div class="row" id="main-landing">
	<h1><a href="/">{{ HTML::image('/assets/img/logo.png', 'Murch Collective')}}</a></h1>
</div>
<div class="row" id="signup">
	<div class="alert alert-info" style="margin-top:30px">Please login to continue</div>
	<div class="col-md-5">
		<h2>Login</h2>
		{{ Form::open(array('class' => 'form-horizontal','url' => '/user/login')) }}
			<div class="form-group">
				<label class="col-sm-3 control-label" for="username">Username or Email</label>
				<div class="col-sm-9">
					<input class="form-control" placeholder="Username or Email" type="text" name="email" id="username" value="">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label" for="password">Password</label>
				<div class="col-sm-9">
					<input class="form-control" placeholder="Password" type="password" name="password" id="password">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-9">
					<button type="submit" class="btn btn-primary btn-block">Login</button>
					<a href="/user/forgot" style="margin-top:10px" class="pull-right">Forget password?</a>
				</div>
			</div>
		</form>
	</div>
	<div class="col-md-5 col-md-offset-2">
		<h2>Signup</h2>
		{{ Form::open(array('class' => 'form-horizontal','url' => '/user')) }}
			<div class="form-group">
				<label class="col-sm-3 control-label" for="username">Username</label>
				<div class="col-sm-9">
					<input class="form-control" placeholder="Username" type="text" name="username" id="username" value="">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label" for="email">Email</label>
				<div class="col-sm-9">
					<input class="form-control" placeholder="Email" type="text" name="email" id="email" value="{{$email or ''}}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label" for="twitter">Artist twitter</label>
				<div class="col-sm-9">
					<input class="form-control" placeholder="@twitterHandle" type="text" name="twitter" id="twitter" value="">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label" for="password">Password</label>
				<div class="col-sm-9">
					<input class="form-control" placeholder="Password" type="password" name="password" id="password">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-9">
					<button type="submit" class="btn btn-primary btn-block">Signup</button>
				</div>
			</div>
		</form>
	</div>
</div>
<a id="mail-link" href="mailto:info.murch.co@gmail.com" target="_blank">Contact <span class="fa fa-envelope"></span></a>

@stop
