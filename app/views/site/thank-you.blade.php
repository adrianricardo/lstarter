@extends('site.layouts.landing')

@section('scripts')
<link href='/assets/css/animate.css' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:600' rel='stylesheet' type='text/css'>
<script src="/assets/compiled/js/designer.js"></script>
@endsection


{{-- Content --}}
@section('content')
<div class="row" id="main-landing">
	<h1><a href="/">{{ HTML::image('/assets/img/logo.png', 'Murch Collective')}}</a></h1>
</div>
<div class="row" id="thank-you">
	<h2>Thank you!</h2>
	<h3>We are reviewing your account</h3>
	<h4>Murch.co is currently invite-only. We will process your account and contact you as soon as a store is ready!</p>
</div>

<a id="mail-link" href="mailto:info.murch.co@gmail.com" target="_blank">Contact <span class="fa fa-envelope"></span></a>

@stop
