@extends('site.layouts.default')

{{-- Content --}}
@section('content')
	<div class="page-header">
		<h3>
			{{$user->display_name}}

			<div class="pull-right">
				<a href="{{{ URL::to('products/create') }}}" class="btn btn-small btn-info iframe"><span class="fa fa-plus"></span> Create</a>
			</div>
		</h3>
	</div>
	<?php print_r($user); ?>
@foreach ($products as $product)
	<div class="row">
		<div class="col-md-8">
			<!-- Post Title -->
			<div class="row">
				<div class="col-md-8">
					<h4><strong><a href="#">{{$product->garment_product_id}}</a></strong></h4>
					<ul>
						<li>Product ID: {{$product->garment_product_id}}</li>
						<li>Type: {{$product->type}}</li>
						<li>Color: {{$product->color}}</li>
						<li>Print type: {{$product->print_type}}</li>
				</div>
			</div>
			<!-- ./ post title -->
		</div>
	</div>

<hr />
@endforeach


@stop
