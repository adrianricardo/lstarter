@extends('site.layouts.default')

{{-- Content --}}
@section('content')
	<div class="page-header">
		<h3>{{$product->name." - $".$product->price}}</h3>
	</div>
	<ul id="products-container">
 	<li class="">
		<img src="{{url('assets/img/products/default.png')}}" />
		<form method="post" action="/products/buy/{{$product->id}}">
			<input type="submit" class="btn btn-primary btn-lg" value="Buy" />
		</form>
	</li>
 	</ul>


@stop
