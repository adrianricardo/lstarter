<?php 
	if(!isset($confirm)){
		$confirm = false;
	}
	$colspan = ($confirm) ? '3' : '3';
	$class = ($confirm) ? 'col-md-9' : '';
?>
@if($cart)
	<div id="cart" class="{{$class}}">
		<table class="table">
			<thead>
				<tr>
					<th>Product</th>
					<th>Qty</th>
					<th class="phone-hidden">Item Price</th>
					<th style="width:100px">Subtotal</th>
					@if(!$confirm)
						<th style="width:100px"></th>
					@endif
				</tr>
			</thead>

			<tbody>
			 @foreach($cart as $row)
				<tr>
					<td>
						<p><strong>{{$row->name}}</strong></p>
						<p>{{($row->options->has('size') ? 'size: ' . $row->options->size : '')}}</p>
						@if($confirm)
							<p><a class="cart-change" href="/checkout/">(Change)</a></p>
						@endif
					</td>
					<td>{{$row->qty}}</td>
					<td class="phone-hidden">${{$row->price}}</td>
					<td>${{$row->subtotal}}</td>
					@if(!$confirm)
						<td>
							{{ Form::open(array('class'=>'form-horizontal','url' => '/checkout/remove/'.$row->rowid)) }}
								<input type="submit" class="btn btn-default" value="Remove" />
							</form>
						</td>
					@endif
				</tr>
			@endforeach
				<tr>
					@if($international_shipping)
						<td colspan="{{$colspan}}">International Shipping:</td>
					@else
						<td colspan="{{$colspan}}">Shipping:</td>
					@endif
					<td>${{$shipping}}</td>
					@if(!$confirm)
						<td></td>
					@endif
				</tr>
				<tr>
					<td colspan="{{$colspan}}">Total:</td>
					<td>${{$total+$shipping}}</td>
					@if(!$confirm)
						<td></td>
					@endif
				</tr>

			</tbody>
		</table>
	</div>
@endif