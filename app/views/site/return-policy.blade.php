@extends($layout)
{{-- Web site Title --}}
@section('title')
{{{ Lang::get('site.contact_us') }}} ::
@parent
@stop

{{-- Content --}}
@section('content')

<div class="page-header">
	<h3>
		Returns/Exchance policy
	</h3>
</div>


All sales are final. No exchanges or refunds. If there is a problem with your order please email us at {{$user->email or 'info.murch.co@gmail.com'}}

@stop
