@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
    <ul>
        <li>count:          {{$latest_quote->count}}</li>
        <li>orderToken:     {{$latest_quote->orderToken}}</li>
        <li>created_at:     {{$latest_quote->created_at}}</li>
    </ul>
    {{-- Quote Product Form --}}
    <form class="form-horizontal" method="post" action="/admin/users/order/{{$user_id}}" autocomplete="off">
        <!-- CSRF Token -->
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <input type="hidden" name="user_id" value="{{ $user_id }}" />
        <!-- ./ csrf token -->

        <!-- Form Actions -->
        <div class="control-group">
            <div class="controls">
                <element class="btn-cancel close_popup">Cancel</element>
                <button type="submit" class="btn btn-danger close_popup">Order</button>
            </div>
        </div>
        <!-- ./ form actions -->
    </form>
@stop
