@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
    {{-- Quote user_id Form --}}
    <form class="form-horizontal" method="post" action="/admin/users/quote/{{$user_id}}" autocomplete="off">
        <!-- CSRF Token -->
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <input type="hidden" name="user_id" value="{{ $user_id }}" />
        <!-- ./ csrf token -->

        <!-- Form Actions -->
        <div class="control-group">
            <div class="controls">
                <element class="btn-cancel close_popup">Cancel</element>
                <button type="submit" class="btn btn-danger close_popup">Quote</button>
            </div>
        </div>
        <!-- ./ form actions -->
    </form>
@stop
