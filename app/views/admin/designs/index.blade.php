@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
	Orders :: @parent
@stop

{{-- Content --}}
@section('content')
	<div class="page-header">
		<h3>
			Design Management

			<div class="pull-right">
				<a href="{{{ URL::to('admin/roles/create') }}}" class="btn btn-small btn-info iframe"><span class="fa fa-plus"></span> Create</a>
			</div>
		</h3>
	</div>

	@foreach ($designs as $design)
		<div class="design" style="margin-top:20px;">
			<span><b>id:</b> {{$design->id}}</span>
			<span><b>vendor_designId:</b> {{$design->vendor_designId or "NULL"}}</span>
			<span><b>name:</b> {{$design->name}}</span>
			<span><b>type:</b> {{$design->type}}</span>
			<div style="margin-left:20px;">
				@foreach($design->sides as $side)
					<span><b>side:</b> {{$side->side}}</span>
					<span><b>artwork:</b> {{$side->artwork}}</span>
					<span><b>proof:</b> {{$side->proof}}</span>
					<span><b>colors:</b> {{$side->colors or "NULL"}}</span>
					<span><b>dimensions_width:</b> {{$side->dimensions_width or "NULL"}}</span>
					<span><b>dimensions_height:</b> {{$side->dimensions_height or "NULL"}}</span>
					<span><b>position_horizontal:</b> {{$side->position_horizontal}}</span>
					<span><b>position_offset_top:</b> {{$side->position_offset_top or "NULL"}}</span>
					<span><b>position_offset_bottom:</b> {{$side->position_offset_bottom or "NULL"}}</span>
				@endforeach
			</div>
			@if(!$design->vendor_designId)
				<div>
					{{ Form::open(array('url' => array('admin/designs/submit', $design->id), 'method' => 'post')) }}
						<button type="submit" class="btn btn-default">Submit</button>
					{{ Form::close() }}
				</div>
			@endif
		</div>
	@endforeach

	
@stop