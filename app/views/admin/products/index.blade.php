@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
	Proucts :: @parent
@stop

{{-- Content --}}
@section('content')
	<div class="page-header">
		<h3>
			Product Management

			<div class="pull-right">
				<a href="{{{ URL::to('admin/products/create') }}}" class="btn btn-small btn-info iframe"><span class="fa fa-plus"></span> Create</a>
			</div>
		</h3>
	</div>

	<table id="products" class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-2">{{{ Lang::get('admin/roles/table.name') }}}</th>
				<th class="col-md-1">Creator</th>
				<th class="col-md-1">Design</th>
				<th class="col-md-1">Act</th>
				<th class="col-md-1">Price</th>
				<th class="col-md-1"># Sold</th>
				<th class="col-md-1">Revenue</th>
				<th class="col-md-1">Quote</th>
				<th class="col-md-1">Per Shirt</th>
				<th class="col-md-1">First purchase</th>
				<th class="col-md-1">{{{ Lang::get('admin/roles/table.created_at') }}}</th>
				<th class="col-md-2">{{{ Lang::get('table.actions') }}}</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
@stop

{{-- Scripts --}}
@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {
				oTable = $('#products').dataTable( {
				"sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
				"sPaginationType": "bootstrap",
				"oLanguage": {
					"sLengthMenu": "_MENU_ records per page"
				},
				"bProcessing": true,
		        "bServerSide": true,
		        "sAjaxSource": "{{ URL::to('admin/products/data') }}",
		        "fnDrawCallback": function ( oSettings ) {
	           		$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	     		}
			});
		});
	</script>
@stop