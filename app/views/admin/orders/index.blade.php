@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
	Orders :: @parent
@stop

{{-- Content --}}
@section('content')
	<div class="page-header">
		<h3>
			Order Management

			<div class="pull-right">
				<a href="{{{ URL::to('admin/roles/create') }}}" class="btn btn-small btn-info iframe"><span class="fa fa-plus"></span> Create</a>
			</div>
		</h3>
	</div>

	<table id="orders" class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1">Id</th>
				<th class="col-md-1">Shipping</th>
				<th class="col-md-1">Total</th>
				<th class="col-md-2">Products</th>
				<th class="col-md-1">Submitted</th>
				<th class="col-md-1">User Id</th>
				<th class="col-md-3">Address</th>
				<th class="col-md-2">Date</th>
				{{--<th class="col-md-2">{{{ Lang::get('table.actions') }}}</th>--}}
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
@stop

{{-- Scripts --}}
@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {
				oTable = $('#orders').dataTable( {
				"sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
				"sPaginationType": "bootstrap",
				"oLanguage": {
					"sLengthMenu": "_MENU_ records per page"
				},
				"bProcessing": true,
		        "bServerSide": true,
		        "sAjaxSource": "{{ URL::to('admin/orders/data') }}",
		        "fnDrawCallback": function ( oSettings ) {
	           		$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	     		}
			});
		});
	</script>
@stop