<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Basic Page Needs
		================================================== -->
		<meta charset="utf-8" />
		<title>
			@section('title')
			{{$user->display_name or $user->username }} | Murch.co
			@show
		</title>
		<meta name="author" content="Murch.co" />
		<meta name="description" content="Official {{$user->display_name}} merchandise from Murch.co. Support your favorite artist!" />
		<meta property="og:url" content="{{Request::url()}}"/>
		<meta property="og:title" content="{{$user->display_name}} | Murch.co"/>
		<meta property="og:description" content="Official {{$user->display_name}} merchandise from Murch.co. Support your favorite artist!"/>
		@if(isset($product) && count($product->printSides))
			@foreach ($product->printSides as $print)
				<?php $imageurl = $print->proof; ?>
				<?php break; ?>
			@endforeach
		@else
			<?php $imageurl = asset('img/products/default.png'); ?>
		@endif
		<meta property="og:image" content="{{$imageurl}}"/>

		<!-- Mobile Specific Metas
		================================================== -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- CSS
		================================================== -->
		<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:300' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="/compiled/public/css/{{ AppHelper::asset_path('public.css') }}">
		<link rel="stylesheet" href="/compiled/public/css/public.css">

		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

        

		<style>
		@section('styles')
		@show
		</style>

		@if($user->storeOptions->background_image)
			<style>
				body{
						background: url('{{$user->storeOptions->background_image}}') repeat;
				}
			</style>
		@endif

		@if($user->storeOptions->custom_css)
			<style>
				{{$user->storeOptions->custom_css}}
			</style>
		@endif


		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->


		<!-- Favicons
		================================================== -->
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{{ asset('assets/ico/apple-touch-icon-144-precomposed.png') }}}">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{{ asset('assets/ico/apple-touch-icon-114-precomposed.png') }}}">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{{ asset('assets/ico/apple-touch-icon-72-precomposed.png') }}}">
		<link rel="apple-touch-icon-precomposed" href="{{{ asset('assets/ico/apple-touch-icon-57-precomposed.png') }}}">
		<link rel="shortcut icon" href="{{{ asset('assets/ico/favicon.png') }}}">

		<script src="/compiled/public/js/{{ AppHelper::asset_path('public.js') }}"></script>
		<script src="/assets/js/store.js"></script>

		<!-- Javascripts
		================================================== -->
	</head>

	<body id="seller-profile" class="{{{$class or ''}}}">
		<!-- To make sticky footer need to wrap in a div -->
		<div id="wrap">
		<!-- Navbar -->
		 <div class="container">
            <ul class="nav navbar-nav profile-area">
				<li {{ (Request::is('/') ? ' class="active"' : '') }}>
				<a class="profile-photo" href="http://{{$user->username}}.{{Config::get('app.sitename')}}">
					<img src="{{{$user->image or "/assets/img/seller/no_photo.jpg"}}}" />
					@if($user->storeOptions->show_display_name)
						{{$user->display_name or $user->username}}
					@endif
				</a>
				<?php //<span class="brand">.murch.co</span> ?>
				</li>
			</ul>

            <ul class="nav navbar-nav pull-right right-menu">
            	<?php /*
                @if (Auth::check())
                <li><a href="{{{ URL::to('user') }}}">{{ Auth::user()->username }}</a></li>
                <li><a href="{{{ URL::to('user/logout') }}}">Logout</a></li>
                @else
                <li {{ (Request::is('user/login') ? ' class="active"' : '') }}><a href="{{{ URL::to('user/login') }}}">Login</a></li>
                <li {{ (Request::is('user/register') ? ' class="active"' : '') }}><a href="{{{ URL::to('user/create') }}}">{{{ Lang::get('site.sign_up') }}}</a></li>
                @endif
                */ ?>
                <li class="cart"><a href="{{{secure_url('checkout/cart') }}}">
                @if(Cart::count(false) == 0)
					{{'$0.00'}}
				@else
					{{'$'.Cart::total()}}
				@endif
				</a></li>
            </ul>
			<!-- ./ nav-collapse -->
		</div>
		<!-- ./ navbar -->

		<!-- Container -->
		<div class="container">
			<!-- Notifications -->
			@include('notifications')
			<!-- ./ notifications -->

			<!-- Content -->
			@yield('content')
			<!-- ./ content -->
		</div>
		<!-- ./ container -->

		<!-- the following div is needed to make a sticky footer -->
		<div id="push"></div>
		</div>
		<!-- ./wrap -->


	    <div id="footer">
	      <div class="container">
	        @if($user->twitter or $user->facebook or $user->instagram)
				<div id="social-buttons">
					<ul>
						@if($user->twitter)
							<li><a href="http://twitter.com/{{$user->twitter}}" target="_blank"><span class="fa fa-twitter"></span></a></li>
						@endif
						@if($user->instagram)
							<li><a href="http://instagram.com/{{$user->instagram}}" target="_blank"><span class="fa fa-instagram"></span></a></li>
						@endif
						@if($user->facebook)
							<li><a href="http://facebook.com/{{$user->facebook}}" target="_blank"><span class="fa fa-facebook"></span></a></li>
						@endif
					</ul>
				</div>
			@endif
			<p class="credit">Powered by <a href="http://murch.co">Murch.co</a>.</p>
	      </div>
	    </div>
	    @yield('scripts')
	</body>
</html>
