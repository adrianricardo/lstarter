<?php

class AdminProductsController extends AdminController
{

    

    /**
     * Inject the models.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {

        // Show the page
        return View::make('admin/products/index');
    }


    /**
     * Show a list of all the products formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $products = Product::select(array('products.id', 'products.name', 'products.user_id', 'products.design_id as design_id',
            'products.active',
            'products.price', 'products.id as sold', 'products.id as revenue', 'products.id as quote',
            'products.id as per_shirt', 'products.id as firstsold', 
            'products.created_at'));

        return Datatables::of($products)
        ->edit_column('created_at','{{{ date("m/d/y",strtotime($created_at)) }}}')
        ->edit_column('sold', '{{{ DB::table("order_items")->where("product_id", "=", $id)->where("sio_order_id", "=", null)->sum("quantity")  }}}')
        ->edit_column('revenue', '${{{ DB::table("order_items")->where("product_id", "=", $id)->where("sio_order_id", "=", null)->select(DB::raw("SUM(price_at_sale*quantity)as rev"))->pluck("rev")  }}}')
        ->edit_column('quote', '
            ${{{ DB::table("sio_quotes")
            ->orderBy("created_at", "desc")
            ->where("product_id", "=", $id)
            ->where("count", "=", DB::table("order_items")->where("product_id", "=", $id)->where("sio_order_id", "=", null)->sum("quantity"))
            ->pluck("murch_total")  }}}')
        ->edit_column('per_shirt', '
            ${{{ DB::table("sio_quotes")
            ->orderBy("created_at", "desc")
            ->take(1)
            ->where("product_id", "=", $id)
            ->where("count", "=", DB::table("order_items")->where("product_id", "=", $id)->where("sio_order_id", "=", null)->sum("quantity"))
            ->pluck("murch_price_per_shirt")  }}}')
        ->edit_column('firstsold', '{{{ date("m/d/y",strtotime(DB::table("order_items")->where("product_id", "=", $id)->where("sio_order_id", "=", 
            DB::table("order_items")->where("product_id", "=", $id)->where("sio_order_id", "=", null)->count()
           )->min("created_at")))  }}}')
        ->add_column('actions', '<a href="{{{ URL::to("admin/roles/" . $id . "/edit" ) }}}" class="btn btn-xs btn-default">{{{ Lang::get("button.edit") }}}</a>
                                <a href="{{{ URL::to("admin/users/quote/" . $user_id  ) }}}" class="btn btn-xs btn-info">Quote</a>
                                <a href="{{{ URL::to("admin/users/clear-quotes/" . $user_id  ) }}}" class="btn btn-xs btn-info">Clear</a>
                                @if(DB::table("sio_quotes")
                                    ->where("product_id", "=", $id)
                                    ->where("count", "=", DB::table("order_items")->where("product_id", "=", $id)->where("sio_order_id", "=", null)->sum("quantity"))
                                    ->pluck("murch_total")
                                )
                                    <a href="{{{ URL::to("admin/users/order/" . $user_id  ) }}}" class="btn btn-xs btn-success">Order</a>
                                @endif
                    ')

        ->remove_column('id')

        ->make();
    }
}
