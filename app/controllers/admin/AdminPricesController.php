<?php

class AdminPricesController extends AdminController {


    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the products and prices.
     *
     * @return Response
     */
    public function getIndex()
    {

        // Show the page
        return View::make('admin/prices/index');
    }


    /**
     * Show a list of all the orders formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $orders = Checkout::select(
            array('orders.id', 'orders.shipping',  'orders.total', 'orders.id as products', 
                'orders.id as submitted', 'orders.user_id', 
                'orders.user_id as shipping_address', 'orders.created_at', 
                'orders.shipping_name', 'orders.shipping_street1', 'orders.shipping_street2',
                'orders.shipping_city', 'orders.shipping_state', 'orders.shipping_zip', 'orders.shipping_country' 
            ));

        return Datatables::of($orders)
        ->edit_column('products', '{{{ json_encode(DB::table(\'order_items\')->where(\'order_id\', \'=\', $id)->lists(\'product_id\'))  }}}')
        ->edit_column('submitted', ' 
            @if (DB::table(\'order_items\')->where(\'order_id\', \'=\', $id)->lists(\'sio_order_id\')
            == DB::table(\'order_items\')->where(\'order_id\', \'=\', $id)->lists(\'product_id\'))
                true
            @else 
                 false
            @endif
        ')
        ->edit_column('created_at','{{{ date("m/d/y",strtotime($created_at)) }}}')
      

        ->remove_column('shipping_name')
        ->remove_column('shipping_street1')
        ->remove_column('shipping_street2')
        ->remove_column('shipping_city')
        ->remove_column('shipping_state')
        ->remove_column('shipping_zip')
        ->remove_column('shipping_country')

        ->make();
    }

}