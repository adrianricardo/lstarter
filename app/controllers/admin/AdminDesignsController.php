<?php

class AdminDesignsController extends AdminController {


	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{

		// Grab all the groups
		$designs = Design::with('sides')->get();

		// Show the page
		return View::make('admin/designs/index', compact('designs'));
	}


	/**
	 * Submits a design and saves the vendor design id
	 *
	 * @return Response
	 */
	public function postSubmit($id)
	{

		// Grab all the groups
		$design = Design::where('id',$id)->whereNull('vendor_designId')->with('sides')->first();

		$design_array = Array(
			'type'=> $design->type,
		);

		foreach ($design->sides as $side) {
			$side_array['artwork'] = $side->artwork;
			$side_array['position']['horizontal'] = $side->position_horizontal;

			//dimensions
			if(!empty($side->dimensions_width)){
				$side_array['dimensions']['width'] = $side->dimensions_width;
			}else{
				$side_array['dimensions']['height'] = $side->dimensions_height;
			}

			//position
			if(!empty($side->position_offset_top)){
				$side_array['position']['offset']['top'] = $side->position_offset_top;
			}else{
				$side_array['position']['offset']['bottom'] = $side->position_offset_bottom;
			}

			if($side->type=="screenprint"){
				if(empty($side->colors)){
					return false;
				}

				$color_array = explode(',', $side->colors);
				foreach ($color_array as $color) {
					$side_array['colors'][] = $color;
				}
			}

			$design_array['sides'][$side->side] = $side_array;
		}

		$options = Array(
			CURLOPT_USERPWD => ':'.Config::get('settings.sp_api')
		);

		//get quote
		try {
			$resp = Curl::post('https://api.scalablepress.com/v2/design',$design_array,$options)->body;
		} catch (Passioncoder\SimpleCurl\Exception $e) {
			print $e->getMessage();
			return false;
		}

		if(property_exists($resp, 'statusCode') && $resp->statusCode == 400){
			//There was an ERROR
			sd($resp);
		}
		

		if($resp->designId){
			$design->vendor_designId = $resp->designId;
			$design->save();
		}

	}

}