<?php

class AdminOrdersController extends AdminController {


    /**
     * Order Model
     * @var Order
     */
    protected $order;


    /**
     * Inject the models.
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        parent::__construct();
        $this->order = $order;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {

        // Grab all the groups
        $orders = $this->order;

        // Show the page
        return View::make('admin/orders/index', compact('orders'));
    }


    /**
     * Show a list of all the orders formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $orders = Checkout::select(
            array('orders.id', 'orders.shipping',  'orders.total', 'orders.id as products', 
                'orders.id as submitted', 'orders.user_id', 
                'orders.user_id as shipping_address', 'orders.created_at', 
                'orders.shipping_name', 'orders.shipping_street1', 'orders.shipping_street2',
                'orders.shipping_city', 'orders.shipping_state', 'orders.shipping_zip', 'orders.shipping_country' 
            ));

        return Datatables::of($orders)
        ->edit_column('products', '{{{ json_encode(DB::table(\'order_items\')->where(\'order_id\', \'=\', $id)->lists(\'product_id\'))  }}}')
        ->edit_column('submitted', ' 
            @if (DB::table(\'order_items\')->where(\'order_id\', \'=\', $id)->lists(\'sio_order_id\')
            == DB::table(\'order_items\')->where(\'order_id\', \'=\', $id)->lists(\'product_id\'))
                true
            @else 
                 false
            @endif
        ')
        ->edit_column('created_at','{{{ date("m/d/y",strtotime($created_at)) }}}')
        ->edit_column('shipping_address', '{{ $shipping_name."
            <br />".$shipping_street1." ".$shipping_street2."
            <br />".$shipping_city.", ".$shipping_state." ".$shipping_zip."
            <br />".$shipping_country
        }}')
       // ->add_column('actions', '<a href="{{{ URL::to(\'admin/roles/\' . $id . \'/edit\' ) }}}" class="iframe btn btn-xs btn-default">{{{ Lang::get(\'button.edit\') }}}</a>
        //                        <a href="{{{ URL::to(\'admin/roles/\' . $id . \'/delete\' ) }}}" class="iframe btn btn-xs btn-danger">{{{ Lang::get(\'button.delete\') }}}</a>
        //            ')

        ->remove_column('shipping_name')
        ->remove_column('shipping_street1')
        ->remove_column('shipping_street2')
        ->remove_column('shipping_city')
        ->remove_column('shipping_state')
        ->remove_column('shipping_zip')
        ->remove_column('shipping_country')

        ->make();
    }

}