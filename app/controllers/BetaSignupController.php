<?php

class BetaSignupController extends BaseController {

   
    public function __construct()
    {
        parent::__construct();
    }
    
	/**
	 * Landing page
	 *
	 * @return View
	 */
	public function getIndex()
	{

		// Show the page
		return View::make('site/landing');
	}

	/**
	 * Landing page form submission
	 *
	 * @return View
	 */
	public function postIndex()
	{
		if(Input::get('email') == ""){
			return Redirect::to('/')
                ->with( 'error', 'Please enter an email address' );
		}

		$validator = Validator::make(
			Input::all(),
		    array('email' => 'required|unique:beta_signups')
		);

		if ($validator->passes()){
		    $signup = new BetaSignup();
			$signup->email = Input::get( 'email' );
			$signup->artist_twitter = Input::get( 'twitter' );

	        // Save if valid. Password field will be hashed before save
	        $signup->save();

	        return Redirect::to('/signup?email='.Input::get( 'email' ));
		}

		$error = "Email already exists";

		if(Input::get('email') == ""){
			$error = "Please enter an email";
		}

        // Get validation errors (see Ardent package)
        return Redirect::to('/signup');
        
	}

	public function getLogin()
	{		
		// Show the page
		return View::make('site/login');
	}

	public function getSignup()
	{
		return View::make('site/signup')->with('email',Input::get("email"));
		
		// Show the page
		return View::make('site/signup');
	}


	public function getThankYou()
	{

		// Show the page
		return View::make('site/thank-you');
	}
}
