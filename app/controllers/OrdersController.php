<?php

class OrdersController extends BaseController {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |   Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function getIndex()
    {
        $data['orders'] = Orders::all();
        return View::make('orders.orders',$data);
    }
    public function getView($id){
        $data['orders'] = Orders::where('product_id', '=', $id)->get();
        $data['product_id'] = $id;
        return View::make('orders.orders',$data);
    }
    public function getQuote($id){
        $orders = new Orders();
        $data['quote'] = $orders->getQuote($id);
        $data['orders'] = Orders::where('product_id', '=', $id)->get();
        $data['product_id'] = $id;
        return View::make('orders.quote',$data);
    }
    public function getSubmit($product_id){
        $orders = new Orders();
        $data['order'] = $orders->submitOrder($product_id);
        $data['product_id'] = $product_id;
        return View::make('orders.order_placed',$data);
    }

}