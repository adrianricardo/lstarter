<?php

class ProductController extends BaseController {

	/**
     * Product Model
     * @var Product
     */
	protected $product;

    /**
     * User Model
     * @var User
     */
    protected $user;


    /**
     * Inject the models.
     * @param Product $product
     * @param User $user
     */
    public function __construct(Product $product,User $user)
    {
    	parent::__construct();
    	$this->product = $product;
    	$this->user = $user;
    }


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex($id)
	{
		// Grab all the users
		$product = Product::where('id', $id)->get()->first();

        // Show the page
		return View::make('site/products/index', compact('product'));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getView($id)
	{
		// Grab all the users
		$product = Product::where('id', $id)->get()->first();

		$user = Auth::user();
		if(empty($user->id)){
			$email = false;
		}else{
			$email = $user->email;
		}

        $user = User::where('id', $product->user_id)->get()->first();
        if($user->storeOptions->currency_converter){
        	$currencies = Currency::getCurrencies();
        }else{
        	$currencies = false;
        }

        // Show the page
		return View::make('seller/profile/single_product', compact('product','user','email','currencies'));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getBuy($id)
	{
		// Grab all the users
		$product = Product::where('id', $id)->get()->first();

        // Show the page
		return View::make('site/products/buy', compact('product'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		if (Auth::check())
		{
			$user = $this->user->currentUser();
			$canSellProducts = $user->can('sell_products');
			if ( ! $canSellProducts)
			{
				return Redirect::to('/')->with('error', $this->user->currentUser());
			}
			return View::make('site/products/create');
		}
		return Redirect::to('/')->with('error', $this->user->currentUser());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getAdmin()
	{
		if (Auth::check())
		{
			$user = $this->user->currentUser();
			$canSellProducts = $user->can('sell_products');
			if ($canSellProducts)
			{
				return View::make('site/products/create');
				
			}
		}
		return Redirect::to('/')->with('error', $this->user->currentUser());
		
		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postCreate()
	{
		$user = $this->user->currentUser();
		$canSellProducts = $user->can('sell_products');
		if ( ! $canSellProducts)
		{
			return Redirect::to('/')->with('error', 'You need to be registered as a seller');
		}

		// Declare the rules for the form validation
		$rules = array(
			'garment_product_id' => 'required'
			);

		// Validate the inputs
		$validator = Validator::make(Input::all(), $rules);

		// Check if the form validates with success
		if ($validator->passes())
		{
			// Save the product
			$guid =  AppHelper::getGUID();

			$product = new Product(Input::except('_token'));
			$user_id = Auth::user()->id;
			$product->user_id = $user_id;
			$product->guid = $guid;

			$s3 = AWS::get('s3');

			var_dump(Input::file('artwork'));
				die();

			return false;

			// Was the comment saved with success?
			if($product->save())
			{

				foreach(Input::file('artwork') as $file){
					die();
					$rules = array(
						'file' => 'required|mimes:png,jpeg,svg,ai,eps|max:20000'
						);
					$validator = \Validator::make(array('file'=> $file), $rules);
					if($validator->passes()){
						$ext = $file->guessClientExtension(); // (Based on mime type)
						$path = $user_id . "/";
						$full_path = $path.$product->guid.".".$ext; 
						$result = $s3->putObject(array(
							'Bucket'     => $_ENV['uploaded_artwork_bucket'],
							'Key'        => $full_path,
							'SourceFile' => $file,
							'ContentType' => 'image/jpeg'
							));

						$artwork = new UploadedArtwork;
						$artwork->product_id = $product->id;
						$artwork->file = $result['ObjectURL'];
						$artwork->save();
					}else{
						//Does not pass validation
						$errors = $validator->errors();
					}

        		}

				// Redirect to this blog post page
        //return Redirect::to('/products')->with('success', 'Your item was created with success.');
    }

			// Redirect to this blog post page
    return Redirect::to('/products/create')->with('error', 'There was a problem adding your item, please try again.');
}

		// Redirect to this blog post page
return Redirect::to('/products/create')->withInput()->withErrors($validator);
}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}