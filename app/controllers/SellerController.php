<?php

class SellerController extends BaseController {


    /**
     * Initializer.
     *
     * @return \SellerController
     */
    public function __construct()
    {
        parent::__construct();
        // Apply the seller auth filter
        $this->beforeFilter('seller');
    }



}