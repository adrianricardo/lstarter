<?php

class SellerOrdersController extends SellerController
{

    

    /**
     * Inject the models.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the products this seller owns
     *
     * @return Response
     */
    public function getIndex()
    {
 
        $class = "orders";

        // Show the page
        return View::make('seller/orders/index', compact('class'));
    }

   


    /**
     * Show a list of all the products formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $user = Auth::user();
        $id_array = array();
        $product_ids = Product::where('user_id',Auth::user()->id)->select('id')->get();
        foreach ($product_ids as $product) {
            $id_array[] = $product->id;
        }
        $orders =  OrderItem::whereIn('product_id', $id_array)
            ->select(array('order_items.order_id as id', 'order_items.order_id as name', 'order_items.quantity', 
            'order_items.price_at_sale as price', 'order_items.order_id as state',
            'order_items.created_at'));

        return Datatables::of($orders)
        ->edit_column('name', '{{{ DB::table("orders")->where("id", "=", $id)->select("billing_name")->pluck("billing_name")  }}}')
        ->edit_column('state', '{{{ DB::table("orders")->where("id", "=", $id)->select("billing_state")->pluck("billing_state")  }}}')

        ->remove_column('id')

        ->make();
    }

}
