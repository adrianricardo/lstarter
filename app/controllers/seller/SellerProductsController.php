<?php

class SellerProductsController extends SellerController
{

	

	/**
	 * Inject the models.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Display a listing of the products this seller owns
	 *
	 * @return Response
	 */
	public function getIndex()
	{

		// Grab the users
		$user = Auth::user();

		//redirect if hasn't agreed to terms
		if (!$user->agreed_to_terms)
		{
			return Redirect::to('/user/terms-check');
		}

		$productQuotes = PrintQuote::getQuotes();
		$userQuote = UserQuote::getUserQuote();
		$revenueFromCurrentBatch = orderItem::revenueFromCurrentBatch();

		$products = Product::where('user_id', $user->id)
			->where('draft',0)
			->with('orderItems')
			->with('design')
			->with('garment')
			->orderBy('active','desc')
			->get();

		$orderCount = OrderItem::pendingPrint()->sum('quantity');

		$payout_array =  DB::Table('sio_orders')
			->join('users', 'sio_orders.user_id', '=', 'users.id')
			->join('payouts', 'sio_orders.payout_id', '=', 'payouts.id')
			->where('users.id', $user->id)
			->groupBY('payouts.id')
			->get();

		$payouts = array();
		foreach ($payout_array as $payout) {
			$payouts[$payout->user_id] = $payout;
		}

		//get balance
		$balance = Seller::payoutBalance();

		$class = "products";


		// Show the page
		return View::make('seller/products/index', compact('products','productQuotes','class','payouts','balance','orderCount','userQuote','revenueFromCurrentBatch'));
	}

	/**
	 * Display form for creating new product
	 *
	 * @return Response
	 */
	public function getAddDesign()
	{
		return View::make('seller/products/add-design');
	}

	/**
	 * Display form for creating new product
	 *
	 * @return Response
	 */
	public function getCreate()
	{
	   $garments = DB::select('select * from garments where active = 1');
	   $garment_colors = DB::select('select * from garment_colors');

	   foreach ($garment_colors as $color) {
		   $colors[$color->garment_id][] = $color;
	   }


		// Show the page
		return View::make('seller/products/create',compact('garments','colors'));
	}

	public function getCreateNew()
	{
	   $products = DB::select('select * from garments where active = 1');
	   $garment_colors = DB::select('select * from garment_colors');
	   $class = "products";

	   foreach ($garment_colors as $color) {
		   $colors[$color->garment_id][] = $color;
	   }


		// Show the page
		return View::make('seller/products/create_new',compact('products','colors','class'));
	}

	/**
	 * Store a newly created product in db
	 *
	 * @return Response
	 */
	public function postCreate()
	{
		$user = $user = Auth::user();
		$canSellProducts = $user->can('sell_products');
		if ( ! $canSellProducts)
		{
			return Redirect::to('/')->with('error', 'You need to be registered as a seller');
		}

		// Declare the rules for the form validation
		$rules = array(
			'garment_id' => 'required',
			'color' => 'required',
			'artwork' => 'required'
		);

		// Validate the inputs
		$validator = Validator::make(Input::all(), $rules);

		// Check if the form validates with success
		if ($validator->passes())
		{

			/*if (Input::hasFile('photo'))
			{
				$destinationPath = public_path()."/assets/artwork";
				$extension = Input::file('photo')->getClientOriginalExtension();
				$name = Input::file('photo')->getClientOriginalName();
				$size = Input::file('photo')->getSize();
				Input::file('photo')->move($destinationPath,$name.$size.".".$extension);
			} */

			$guid =  AppHelper::getGUID();

			$product = new Product(Input::except('_token','photo'));
			$user_id = Auth::user()->id;
			$product->user_id = $user_id;
			//$product->guid = $guid;

			$s3 = AWS::get('s3');

			// Was the comment saved with success?
			if($product->save())
			{

				foreach(Input::file('artwork') as $file){
					if(file_exists($file)){
						$ext = $file->guessClientExtension(); // (Based on mime type)
						$tmp = $file->getRealPath();
						$path = $product->id . "/";
						$full_path = $path.$guid.".".$ext; 
						$result = $s3->putObject(array(
							'Bucket'     => $_ENV['uploaded_artwork_bucket'],
							'Key'        => $full_path,
							'SourceFile' => $tmp,
							'ContentType' => 'image/jpeg'
							));

						$artwork = new UploadedArtwork;
						$artwork->product_id = $product->id;
						$artwork->file = $result['ObjectURL'];
						$artwork->save();
						unset($artwork);
					}
				}

				// Success, redirect to dash
				return Redirect::to('/')->with('success', 'Thank you! You will be notified after the admin approves your artwork.');
			}

			// Redirect with errors
			 return Redirect::back()->withInput()->withErrors($validator);
		}

		// Redirect with errors
		return Redirect::back()->withInput()->withErrors($validator);
	}

}
