<?php

class SellerDashboardController extends SellerController {

	/**
	 * Seller dashboard
	 *
	 */

	public function getIndex()
	{
        return View::make('seller/dashboard');
	}


	public function getGetPaid()
	{

		if(Auth::user()->recipient_id == ""){
			return View::make('seller/bank_info');
		}else{
			$recipient = Stripe_Recipient::retrieve(Auth::user()->recipient_id);
		}

		//get balance
		$balance = Seller::payoutBalance();


		$class = "get-paid";

		//get payout history

		// Show the page
		return View::make('seller/getpaid', compact('balance','revenue','cost','recipient','class'));
	}

	public function postCreateRecipient($user){

		$result = Seller::createRecipient();

		if($result){
			return Redirect::to('/dashboard/get-paid')
            	->with( 'success', 'Bank account linked!' );
        }else{
        	return Redirect::to('/dashboard/get-paid')
                ->with( 'error', 'There was an error saving your bank info, please try again' );
        }

	}

	public function postTransferFunds(){

		$result = Seller::transferFunds();
		//print_r($result); die();
		if( $result === true){
			return Redirect::to('/dashboard/get-paid')
            	->with( 'success', "Bank transfer scheduled!" );
		}else{
			return Redirect::to('/dashboard/get-paid')
            	->with( 'error', 'There was an error creating the transfer..' );
		}
	}

	/**
     * Show a list of all the products formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getPayouts()
    {
        $payouts = Payout::select(array('payouts.id', 'payouts.amount', 'payouts.created_at'))->where('user_id',Auth::user()->id);

        return Datatables::of($payouts)
        ->edit_column('id','payout id:{{{ $id }}}')
        ->edit_column('amount', '${{{ $amount }}}')
        ->edit_column('created_at','{{{ date("m/d/y",strtotime($created_at)) }}}')

        ->make();
    }
}