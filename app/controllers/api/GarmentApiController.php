<?php

class GarmentApiController extends ApiController {

   
    public function __construct()
    {
        parent::__construct();
    }

    public function Index()
	{

		$garment_color_ids = null;

		if(isset($_GET['product_category'])){
			$garments = Garment::where('active',1)->where('product_category_id',$_GET['product_category'])->with('garmentColors')->get();
		}else{
			$garments = Garment::where('active',1)->with('garmentColors')->get();
		}

		$result_array = AppHelper::formatGarments($garments);

		$response = [ "garment" => $result_array['garments'],
			"garment_colors" => $result_array['garment_colors']
		];
		return Response::json($response);

	}

	public function Show($id)
	{

		$garments = Garment::where('active',1)->where('id',$id)->with('garmentColors')->get();
		$garment_colors = GarmentColors::where('garment_id',$id)->get();

		$result_array = AppHelper::formatGarments($garments);

		$response = [ "garment" => $result_array['garments'],
			"garment_colors" => $result_array['garment_colors']
		];
		return Response::json($response);

		// I think we can delete this, wait a few weeks 9/26/14
		/*$garment_colors = GarmentColors::where('garment_id',$id)->get();
		$garment_colors_list = $garment_colors->lists('id');
		//print_r($garment_colors); die();
		$garment_color_array = array();
		foreach ($garment_colors as $color) {
			$new_color = array(
				'id' => $color->id,
				'name' => $color->name,
				'color' => $color->color,
				'front_image' => $color->front_image
			);
			$garment_color_array[] = $new_color;
		}

		try{
	        $garment = Garment::find($id);
	        $garment_colors = GarmentColors::where('garment_id',$id)->lists('id');
	        $statusCode = 200;
	        $response = [ "garment" => [
	            'id' => (int) $id,
	            'name' => $garment->name,
	            'brand' => $garment->brand,
	            'base_price' => $garment->base_price,
	            'description' => $garment->description,
	            'materials' => $garment->materials,
	            'description' => $garment->description,
	            'garment_category_id' => $garment->garment_category_id,
	            'garment_colors' => $garment_colors_list,
	            'product_category_id' => $garment->product_category_id
	        ],
	        "garment_colors" => $garment_color_array
	        ];
	 
	    }catch(Exception $e){
	        $response = [
	            "error" => "File doesn`t exists"
	        ];
	        $statusCode = 404;
	    }
	    
	    return Response::json($response, $statusCode); */
	}

}
