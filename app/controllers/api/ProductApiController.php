<?php

class ProductApiController extends ApiController {

   
	public function __construct()
	{
		parent::__construct();
	}

	public function getIndex()
	{
		$products = Product::all()->toArray();
		return Response::json( [ 'products' => $products ] );
	}

	public function store()
	{
		$user = $user = Auth::user();
		$user_id = $user->id;
		$canSellProducts = $user->can('sell_products');
		if ( ! $canSellProducts)
		{
			//TODO ERROR HANDLING
			return false;
		}

		$input = Input::json();
		$product = $input->get('product');

		

		$garment_id = $product['garment'];
		$color = $product['color'];


		if(empty($garment_id) || empty($color)){
			return false;
		}

		$product = new Product();
		$product->user_id = $user_id;
		$product->garment_id = $garment_id;
		$product->color = $color;
		$product->draft = 1;

		// Was the product saved with success?
		if($product->save())
		{
			$product = Product::where('id',$product->id)->first();

			$print_1 = PrintQuote::Quote($product->id,1);
			$product->print_1 = $print_1['murch_total'];
			$print_2 = PrintQuote::Quote($product->id,2);
			$product->print_2 = $print_2['murch_total'];
			
			return Response::json( [ 'product' => $product->toArray()] );
		}


		//TODO ERROR HANDLING
		return false;
		

	}


	public function update($product_id)
	{
		$user = $user = Auth::user();
		$user_id = $user->id;
		
		$product = Product::where('id',$product_id)->where('user_id',$user_id)->first();

		if(!count($product)){
			return false;
		}

		$input = Input::json();
		$productData = $input->get('product');


		$garment_id = $productData['garment'];
		$color = $productData['color'];
		$name = $productData['name'];
		$price = $productData['price'];


		if(!empty($garment_id)){
			$product->garment_id = $garment_id;
		}

		if(!empty($color)){
			$product->color = $color;
		}

		if(!empty($name)){
			$product->name = $name;
		}

		if(!empty($price)){
			$product->price = $price;
		}

		

		// Was the product saved with success?
		if($product->save())
		{
			$print_1 = PrintQuote::Quote($product->id,1);
			$product->print_1 = $print_1['murch_total'];
			$print_2 = PrintQuote::Quote($product->id,2);
			$product->print_2 = $print_2['murch_total'];
			//$quotes = 
			return Response::json( [ 'product' => $product->toArray() ] );
		}


		//TODO ERROR HANDLING
		return false;
		

	}


	public function show($id)
	{
		$user = $user = Auth::user();
		$user_id = $user->id;

		$product = Product::where('id',$id)->where('user_id',$user_id)->first();
		$print_1 = PrintQuote::Quote($product->id,1);
		$product->print_1 = $print_1['murch_total'];
		$print_2 = PrintQuote::Quote($product->id,2);
		$product->print_2 = $print_2['murch_total'];

		$garments = Garment::where('active',1)->where('id',$product->garment_id)->with('garmentColors')->get();
		$product_array = $product->toArray();
		$result_array = AppHelper::formatGarments($garments);

		$product_array['garment'] = $garments->first()->id;

		return Response::json( [ 
			'product' => $product_array,
			"garment" => $result_array['garments'],
			"garment_colors" => $result_array['garment_colors'] 
		] );
	}
}
