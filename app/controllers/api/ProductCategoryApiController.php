<?php

class ProductCategoryApiController extends ApiController {

   
    public function __construct()
    {
        parent::__construct();
    }

    public function Index()
	{
		
		$product_categories = ProductCategory::with('garment')->get();
		$product_category_ids = $product_categories->lists('id');
		$product_categories = $product_categories->toArray();

		$product_category_array = [];
		

		foreach ($product_categories as $value) {
			$product_category = $value;
			$garment_id_list = Garment::where('product_category_id',$value['id'])->lists('id');
			$product_category['garments'] = $garment_id_list;	
			unset($product_category['garment']);
			$product_category_array[] = $product_category;
		}

		$garments = Garment::whereIn('product_category_id',$product_category_ids)->get();

		$result_array = AppHelper::formatGarments($garments);

		$response = [ "product_category" => $product_category_array,
			"garments" => $result_array['garments'],
			"garment_colors" => $result_array['garment_colors']
		];

		return Response::json($response);

	}
}
