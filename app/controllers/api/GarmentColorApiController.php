<?php

class GarmentColorApiController extends ApiController {

   
    public function __construct()
    {
        parent::__construct();
    }

    public function Index()
	{

		if(isset($_GET['garment_id'])){
			if(isset($_GET['name'])){
				$garment_colors = GarmentColor::where('garment_id',$_GET['garment_id'])
				->where('name',$_GET['name'])->get();
			}else{
				$garment_colors = GarmentColor::where('garment_id',$_GET['garment_id'])->get();
			}
		}else{
			$garments = GarmentColor::all()->get();
		}

		$response = [ "garment_colors" => $garment_colors->toArray()];
		return Response::json($response);

	}

	

}
