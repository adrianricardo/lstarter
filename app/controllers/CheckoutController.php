<?php

class CheckoutController extends BaseController {

 

    
    public function __construct()
    {
        parent::__construct();
    }


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return Redirect::secure('/checkout/cart');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function postAdd($id)
	{
		//prepare info for cart
		$product_id = Input::get( 'product_id' );
		$quantity = Input::get( 'quantity' );
		$size = Input::get( 'size' );
		$product = Product::where('id', $product_id)->get()->first();

		//add to cart
		Cart::add($product->id, $product->name, $quantity, $product->price, array('size' => $size));


		return Redirect::secure('/checkout/cart');

		// TODO: when we need accounts for buyers
		/*if (Auth::check() || Session::get('guest')){
        	return Redirect::to('/checkout/cart');
        }
        Session::put('guest',true);
        Session::put('loginRedirect','/checkout/cart');
        return Redirect::to('/checkout/login');*/

		
	}
	

	public function getCart($username = false)
	{	
		$data = array();
		$layout = 'site.layouts.default';
		if($username){
			$userModel = new User;
			$user = $userModel->getUserByUsername($username);
			$data['user'] = $user;
    		$layout = 'profile.layouts.default';
		}
		if(Cart::count(false) == 0){
			$cart = false;
			$total = 0;
		}else{
			$cart = Cart::content();
			$total = Cart::total();
		}

		$shipping = Checkout::calculateShipping();

		if(Session::has('transaction_data')){
			$transaction_data = Session::get('transaction_data');
			$international_shipping = ($transaction_data['stripeShippingAddressCountryCode'] != "US") ? $transaction_data['stripeShippingAddressCountryCode'] : false;
		}else{
			$international_shipping = false;
		}

		if(empty($user->id)){
			$email = false;
        }else{
        	$email = $user->email;
        }

        $class = "cart";

        $data = array_merge($data,compact('cart','email','total','shipping','layout','cart','international_shipping'));

		return View::make('site/checkout/cart',$data)->nest('cart_view','site.partials._cart',$data);
	}

	

	public function postProcess()
	{
		Session::put('transaction_data', $_POST);

		//if not shipping to USA send back to cart with error
		$US_Shipping = ($_POST['stripeShippingAddressCountryCode'] == "US");

		/*if($_POST['stripeShippingAddressCountryCode'] != "US"){
			Session::flash('error', 'We currently only ship within the United States. Be sure to check back soon!');
			return Redirect::back();
		}*/

		//if shipping == billing or shipping outside of US, ask to confirm
		if($this->BillingShippingMatch() || !$US_Shipping){
			return Redirect::secure('/checkout/confirm');
		}

		//charge card and save order
		if(!$this->chargeCard()){
			return Redirect::to('/checkout/cart');
		}

		return Redirect::to('/checkout/summary');
		
	}

	public function getConfirm($username = false)
	{

		if(!Session::has('transaction_data')){
			return Redirect::to('/checkout');
		}

		$layout = 'site.layouts.default';
		$class = "confirm";
		if($username){
			$userModel = new User;
			$user = $userModel->getUserByUsername($username);
			$data['user'] = $user;
			$layout = 'profile.layouts.default';
		}

		if(Cart::count(false) == 0){
			$cart = false;
			$total = 0;
		}else{
			$cart = Cart::content();
			$total = Cart::total();
		}

		$shipping = Checkout::calculateShipping();

		if(empty($user->id)){
			$email = false;
        }else{
        	$email = $user->email;
        }

        $confirm = true;

		$data = Session::get('transaction_data');

		$international_shipping = ($data['stripeShippingAddressCountryCode'] != "US") ? $data['stripeShippingAddressCountryCode'] : false;

		$cart_data = array_merge(compact('cart','total','shipping','confirm','international_shipping'));

		unset($data['_token']);
		unset($data['stripeToken']);

		return View::make('site/checkout/confirm',compact('data','user','class'))->nest('cart_view','site.partials._cart',$cart_data);
		
	}

	public function postConfirm()
	{
		//charge card and save order
		if(!$this->chargeCard()){
			return Redirect::to('/checkout/cart');
		}

		return Redirect::to('/checkout/summary');
	}

	public function postRemove($profile,$rowid){
		$cart = Cart::content();

		Cart::remove($rowid);
		$cart = Cart::content();

		Session::flash('success', 'Item removed');
		return Redirect::to('/checkout/cart');
	}

	public function getSummary($username = false)
	{
		if(Session::has('completed_order')){
			$data['layout'] = 'site.layouts.default';
			if($username){
				$userModel = new User;
				$user = $userModel->getUserByUsername($username);
				$data['user'] = $user;
	    		$data['layout'] = 'profile.layouts.default';
			}
			$order_id = Session::get('completed_order');
			$order = DB::table('orders')
				->where('orders.id',$order_id)
				->first();

			$order_items = DB::table('order_items')
				->where('order_items.order_id',$order_id)
				->join('products', 'order_items.product_id', '=', 'products.id')
				->get();

			$data = array_merge($data,compact('order','order_items'));
			return View::make('site/checkout/summary',$data);
		}
		return Redirect::to('/');
	}

	/* UTILITY */

	private function BillingShippingMatch(){
		$transaction_data = Session::get('transaction_data');

		if($transaction_data['stripeBillingAddressLine1'] != $transaction_data['stripeShippingAddressLine1']){
			return false;
		}
		if($transaction_data['stripeBillingAddressZip'] != $transaction_data['stripeBillingAddressZip']){
			return false;
		}

		//the user checked "same billing/shipping info"
		return true;
	}


	private function chargeCard(){
		if(!Session::has('transaction_data')){
			return false;
		}

		$total = Cart::total();
		$shipping = Checkout::calculateShipping();
		$transaction_data = Session::get('transaction_data');

		// Get the credit card details submitted by the form
		$token = $transaction_data['stripeToken'];

		// Create the charge on Stripe's servers - this will charge the user's card
		try {
			$amount = ($total+$shipping);
			//return View::make('site/test',compact('amount'));

			$charge = Stripe_Charge::create(array(
			  "amount" => $amount*100, 
			  "currency" => "usd",
			  "card" => $token,
			  "description" => Config::get('app.sitename')." Purchase")
			);
		} catch(Stripe_CardError $e) {
			// The card has been declined
			Session::flash('error', 'Card declined');
			return false;
		}
		
		//save order to database
		$order = new Checkout;
		$order_id = $order->saveOrder();

		Session::put('completed_order',$order_id);
		Session::forget('transaction_data');
		return true;
	}




	

}