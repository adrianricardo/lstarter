var gulp = require('gulp');
var gutil = require('gulp-util');
var notify = require('gulp-notify');
var path = require('path');
var less = require('gulp-less');
var autoprefix = require('gulp-autoprefixer');
var minifyCSS = require('gulp-minify-css');
var concat = require('gulp-concat');
var rev = require('gulp-rev');
var rename = require("gulp-rename");
var minifyCSS = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var lessDir = 'public/assets/css/less';

// Compile Sass, autoprefix CSS3,
// and save to target CSS directory
gulp.task('css', function () {
    gulp.src([lessDir + '/master.less','public/assets/css/*.css'],{base: lessDir})
        .pipe(concat('public.css'))
        .pipe(less())
        .pipe(autoprefix('last 10 version'))
        .pipe(minifyCSS())
        .pipe(gulp.dest('public/compiled/public/css'))
        .pipe(rev())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('public/compiled/public/css'))
        .pipe(rev.manifest())
        .pipe(rename({basename: 'public.css'}))
        .pipe(gulp.dest('public/compiled'))
        .pipe(notify('public CSS minified'))
});
 
// Handle js compilation
gulp.task('js', function () {
    return gulp.src(['public/assets/js/vendor/jquery.min.js',
        'public/assets/js/vendor/jquery.dataTables.min.js',       
        'public/assets/js/*.js',
        'vendor/twbs/bootstrap/js/tooltip.js',
        'vendor/twbs/bootstrap/js/*.js'])
        .pipe(concat('public.js'))
        .pipe(require('gulp-debug')())
        .pipe(gulp.dest('public/compiled/public/js'))
        .pipe(rev())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('public/compiled/public/js'))
        .pipe(rev.manifest())
        .pipe(rename({basename: 'public.js'}))
        .pipe(gulp.dest('public/compiled'))
        .pipe(notify('public JS minified'))
});


 

// Keep an eye on Sass, Coffee, and PHP files for changes...
gulp.task('watch', function () {
    gulp.watch('public/assets/css/less/*.less', ['css']);
    gulp.watch('public/assets/js/*.js', ['js']);
});
 
// What tasks does running gulp trigger?
gulp.task('default', ['css', 'js', 'watch']);