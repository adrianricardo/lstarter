main = {
	init: function (){
	},
	initPurchaseAuth: function(){
		
	},
	initBankInfo: function(pubkey){
		Stripe.setPublishableKey(pubkey);
		$('#bank-info').submit(function(event) {
			event.preventDefault();
			var $form = $(this);

			// Disable the submit button to prevent repeated clicks
			$form.find('button').prop('disabled', true);

			Stripe.bankAccount.createToken({
				country: $('.country').val(),
				routingNumber: $('.routing-number').val(),
				accountNumber: $('.account-number').val(),
			}, stripeResponseHandler);

			// Prevent the form from submitting with the default action
			return false;
		}); 
		var stripeResponseHandler = function(status, response) {
			var $form = $('#bank-info');

			if (response.error) {
				// Show the errors on the form
				$form.find('.bank-errors').text(response.error.message);
				$form.find('button').prop('disabled', false);
			} else {
				// token contains id, last4, and card type
				var token = response.id;
				// Insert the token into the form so it gets submitted to the server
				$form.append($('<input type="hidden" name="stripeToken" />').val(token));
				// and submit
				$form.get(0).submit();
			}
		};
	},
	initCreateProduct: function(){
		//show corresponding color list on shirt change
		$('.product-option').on('click',function(){
			$('.color-list').hide();
			var id = $(this).data('id');
			console.log(id);
			$('#color-'+id).show();
		})

		//change shirt color on new selection
		$('.color-list label').on('click',function(){
			var color = $(this).css('background-color');
			$('.sample-product img').css('background-color',color);
		})

		//on first upload check for file
		$('.artwork-upload-file').on('change',function(){
			if($(this).val() != ''){
				$('#upload-more').show();
			}else{
				$('#upload-more').hide();
			}
		});

		//upload-more button
		$('#upload-more').on('click',function(){
			if($('.artwork-upload-file').last().val() == ""){
				alert('You haven\'t selected a file yet');
			}else{
				var html =  '<input class="form-control artwork-upload-file" type="file" name="artwork[]" />';
				$(html).insertAfter('.artwork-upload-file').last();
			}
		})

	}
}