store = {
	init: function (){
		
	},
	initCurrency: function(){
		$('#converter-trigger').on('click',function(){
			$('#converter-container').addClass('open');
		})

		$('#currency-select').on('change',function(){
			var price = $(this).data('price');
			var rate = $(this).val();
			var converted = (rate*price).toFixed(2);

			if(rate == 1){
				$('#converted-price').hide();
			}else{
				$('#converted-price').html('('+converted+')');
			}
		})
	}
}