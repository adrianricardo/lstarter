/*ban = {
	canvas: new fabric.Canvas('c',{ 
		backgroundColor: "white"
	}),
	index: 0,
	activeObject: false,
	init: function(){
		ban.initControls();
		ban.initListeners();
		


	},
	
	initListeners: function(){
		//todo - delete button
		//SELECT
		ban.canvas.on('object:selected', function(e) {
			ban.activeObject = e.target;
			//show respective layer
			var selIndex = ban.activeObject.get('id');
			$('#canvas-objects li').removeClass('active');

			if(ban.canvas.getActiveGroup()){
				ban.canvas.getActiveGroup().forEachObject(function(o){ 
					var id = o.get('id');
					$('#object-'+id).addClass('active');
				});
		    } else {
				$('#object-'+selIndex).addClass('active');
		    }

			//prep controls
			$('.i-text-option,.shape-option').hide();
			var type = e.target.get('type');

			if(type=="i-text"){
				$('.i-text-option').show();
				//fontfamily & color
				var font = ban.activeObject.get('fontFamily');
				var color = ban.activeObject.get('fill');
				$('#active-font').css('font-family',font).html(font);
				$('#color-selector').css('background-color',color);
				
			}
			if(type=="circle" || type=="rect"|| type=="triangle" ){
				$('.shape-option').show();
				var color = ban.activeObject.get('fill');
				$('#color-selector').css('background-color',color);
			}
			ban.canvas.renderAll();
		});

		//UN-SELECT
		// todo - delete button
		ban.canvas.on('selection:cleared', function(e) {
			ban.activeObject = false;
			$('#canvas-objects li').removeClass('active');
			$('.i-text-option,.shape-option').hide();
			$('.pop').popover('hide');
			ban.canvas.renderAll();
		});

	},
	initControls: function(){

			
			//todo
			//deselect on empty space click
			$('#banner-maker,.header,#canvas-wrapper,.edit-panel').on('click',function(e){
				if(this == e.target){
					ban.canvas.deactivateAllWithDispatch();
				}
			})

		

		//delte object
		$('#delete-object').on('click',function(){
			ban.removeActive();
			ban.activeObject.setCoords;
			ban.canvas.renderAll();
		});

		
		//save banner
		$('#save-banner').on('click',function(){
			//deselect all
			ban.canvas.deactivateAllWithDispatch();

			$(this).html('saving').attr('disabled','disabled');

			var user_id = $('#user-id').val();
			if(user_id == '-1'){
				alert('you must be logged in to save a banner, its free!');
				$('#save-banner').html('save').removeAttr("disabled");
				return false;
			}

			//check for name
			var name = $('#banner-name').val();
			var banner_id = $('#banner-id').val();
			if(name == ""){
				alert('Name your banner!');
				$('#banner-name').focus();
				$('#save-banner').html('save').removeAttr("disabled");
				return false;
			}
			var strDataURI = ban.canvas.toDataURL("image/jpeg");
	        strDataURI = strDataURI.substr(22, strDataURI.length);

	        $.ajax({
	            type: 'POST',
	            url: "/banner/create",
	            data: { 
	            	file: strDataURI,
	            	json: JSON.stringify(ban.canvas),
	            	name: name,
	            	url: ban.url,
	            	banner_id: banner_id,
	            	width: ban.canvas.getWidth(),
	            	height: ban.canvas.getHeight(),
	            	_token: $('meta[name="csrf-token"]').attr('content')
        		},
	            success:function(data){
	            	$('#banner-id').val(data);
	            	$('#save-banner').html('save').removeAttr("disabled");
	            	console.log('success');
	            },
	            error:function(data){
	            	$('#save-banner').html('save').removeAttr("disabled");
	              	console.log(data.responseText);
	            }
	        });

		});

		//keypress
		window.addEventListener("keydown",function(e){
			if(ban.activeObject != false){
				console.log('blockin!');
				
				var step = 2;
				if(e.shiftKey){
					step = 20;
				}
		        switch (e.keyCode) {
		            case 8:  // Backspace
		            	e.preventDefault();
		           		e.preventDefault();
		            	ban.removeActive();
		      			break;
		            case 9:  // Tab
		            	console.log('bring up options');
		            	break;
		            case 13: // Enter
		            	e.preventDefault();
		            	break;
	            	case 27: //ESC
	            		ban.canvas.deactivateAllWithDispatch();
	            		break;
		            case 37: // Left
		            	e.preventDefault();
		            	var curr = ban.activeObject.get('left');
		            	
		            	ban.activeObject.set('left',curr-step);
		            	ban.canvas.renderAll();
		            	break;
		            case 38: // Up
		            	e.preventDefault();
		            	var curr = ban.activeObject.get('top');
		            	ban.activeObject.set('top',curr-step);
		            	ban.canvas.renderAll();
		            	break;
		            case 39: // Right
		            	e.preventDefault();
		            	var curr = ban.activeObject.get('left');
		            	ban.activeObject.set('left',curr+step);
		            	ban.canvas.renderAll();
		            	break;
		            case 40: // Down
		            	e.preventDefault();
		            	var curr = ban.activeObject.get('top');
		            	ban.activeObject.set('top',curr+step);
		            	ban.canvas.renderAll();
		            	break;
		            break;

		            default:
		        }
		    }
		},false);
		
		//upload image
		$('#upload-button').on('click',function(){
			$('#upload-file').click();
		})
        var reader = new FileReader();
        document.getElementById('upload-file').onchange = function handleImage(e) {
        	console.log('adding to canvas');
		    var reader = new FileReader();
		    reader.onload = function (event) { 
		        var imgObj = new Image();
		        imgObj.src = event.target.result;
		        imgObj.onload = function () {
		            // start fabricJS stuff
		            var id = ban.canvas.getObjects().length + 1;
		            var image = new fabric.Image(imgObj);
		            image.set({
		            	id: id,
		                left: 10,
		                top: 10,
		                padding: 10,
		                cornersize: 10
		            });
		            //image.scale(getRandomNum(0.1, 0.25)).setCoords();
		            ban.canvas.add(image);
		            ban.addToLayers(id,'image');
		            // end fabricJS stuff
		            
		        }
		        
		    }
		    reader.readAsDataURL(e.target.files[0]);
		}
	},
	removeActive: function(){
		if(ban.canvas.getActiveGroup()){
			ban.canvas.getActiveGroup().forEachObject(function(o){ 
				var id = o.get('id');
				$('#object-'+id).remove();
				ban.canvas.remove(o) 
			});
			ban.canvas.discardActiveGroup().renderAll();
	    } else {
			var id = ban.activeObject.get('id');
			$('#object-'+id).remove();
			ban.activeObject.remove();
	    }
		
		ban.activeObject = false;
	},
	getObjectById: function(id){
		var objsInCanvas = ban.canvas.getObjects();
		for(obj in objsInCanvas){
			if(objsInCanvas[obj].get('id')==id) {
				console.log('found');
				return objsInCanvas[obj]; 
			}
			console.log('notfound' + objsInCanvas[obj].get('id'));
		}
	}
}
*/