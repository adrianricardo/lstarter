App.ApplicationRoute = Ember.Route.extend({
	init: function() {
		this._super();
		$.getJSON( "/auth", function( data ) {
			App.user_id = data;
		});
	},
	setupController: function(controller, model) {
		if(App.newProduct == null){
			App.newProduct = this.store.createRecord('product', {});
		}
	}
});

App.DesignerRoute = Ember.Route.extend({
	currentGarmentId: false,
	model: function(params) {
		if(App.newProduct === null){
			var id = 159
		}else if (typeof App.newProduct.get('garment_id') === 'undefined'){
			var id = 159
		}else{
			var id = App.newProduct.get('garment_id');
		}

		return this.store.find('garment',id);	
	},
	setupController: function(controller, model) {
		
		App.newProduct.set('garment', model);

		controller.set('content', model);
		if(model.id || this.currentGarmentId){
			App.newProduct.set('color',null);
			App.productPreview.set('quote',model.get('base_price'));
			App.productPreview.set('front_image',model.get('garment_colors').get('firstObject').get('front_image'));
			App.newProduct.set('garment_id',model.id);
			this.change = model.id;
		}
		
	}
});


App.ChangeProductRoute = Ember.Route.extend({
	beforeModel: function(){
		if(App.newProduct == null){
			this.transitionTo('/');
		}
	},
	model: function(params) {
		//var garment = App.newProduct.get('garment');
		//return this.store.find('garment',{product_category: garment.get('product_category_id')});
		return this.store.find('garment',params.id);
	},
	setupController: function(controller, model) {
		controller.set('content', model);
	},
});


App.ProductDetailsRoute = Ember.Route.extend({
	productLoaded: false,
	model: function(params){
		return this.store.find('product',params.id);
	},
	setupController: function(controller,model){
		controller.set('content', model);
		

		if(!this.productLoaded){
			//App.newProduct.set('color',model.get('color'));
			App.productPreview.set('front_image',null);
			App.newProduct = model;
			this.productLoaded = true;
		}

		/*if(App.newProduct.get('garment_id') == null){
			App.newProduct = model;
		}else{
			console.log(App.newProduct);
		}*/

		if(App.productPreview.get('front_image') == null){
			this.store.find('garment_color',{garment_id: model.get('garment_id'),name: model.get('color')}).then(function(color){
				App.productPreview.set('front_image',color.objectAt(0).get('front_image'));
			})
		}
	}
});

App.ThankYouRoute = Ember.Route.extend({
	setupController: function(controller,model){
	}
});