var attr = DS.attr,
belongsTo = DS.belongsTo,
hasMany = DS.hasMany;


App.Garment = DS.Model.extend({
	name: attr(),
	brand: attr(),
	description: attr(),
	materials: attr(),
	base_price: attr(),
	garment_category_id: attr(),
	garment_category: belongsTo('garmentCategory'),
	garment_colors: hasMany('garmentColor'),
	product_category_id: attr(),
	product_category: belongsTo('productCategory'),
	product: belongsTo('product')
});


App.GarmentColor = DS.Model.extend({
	name: attr(),
	color: attr(),
	front_image: attr(),
	garment: belongsTo('garment'),
	garment_id: attr()
});


App.Product = DS.Model.extend({
	name: attr(),
	color: attr(),
	price: attr(),
	print_1: attr(),
	print_2: attr(),
	garment: belongsTo('garment'),
	garment_id: attr()
});


App.GarmentCategory = DS.Model.extend({
	name: attr(),
	garment: hasMany('garment')
});


App.ProductCategory = DS.Model.extend({
	name: attr(),
	garment: hasMany('garment'),
	default_garment: attr()
});