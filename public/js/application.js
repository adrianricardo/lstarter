var App = Ember.Application.create();

App.Store = DS.Store.extend({});

App.ApplicationAdapter = DS.RESTAdapter.extend({
  namespace: 'api'
});

App.Product = DS.Model.extend({
    name: DS.attr('string')
});

