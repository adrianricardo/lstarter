App = Ember.Application.create({
	rootElement: '#design-tool-container'
});


App.Router.map(function() {
	this.resource('designer', { path: '/designer' }, function(){
		this.resource('new');
		this.resource('garment', { path: '/garment/:id' });
		this.resource('product-details', { path: '/product-details/:id' });
	});
	this.resource('thank-you');
	this.resource('change-product');
	
});


App.Store = DS.Store.extend({});


App.ApplicationAdapter = DS.RESTAdapter.extend({
	namespace: 'api',
	headers: {
		'csrf_token': $("meta[name='csrf-token']").attr('content'),
	}
});


App.newProduct = null;
var categoryArray = new Array();

App.ProductPreviewObject = Ember.Object.extend({
	front_image: null
});

App.productPreview = App.ProductPreviewObject.create();


Handlebars.registerHelper('ifCond', function(v1, v2, options) {
	if(v1 === v2) {
		return options.fn(this);
	}
	return options.inverse(this);
});


Ember.Handlebars.registerBoundHelper('base_price_helper', function (price) {
	if(price == 0){
		return new Handlebars.SafeString(
			'<a target="_blank" class="no-price" href="/login">Login for pricing</a>'
		);
	}else{
		return '$'+price;
	}
});

