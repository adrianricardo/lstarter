App.NewController = Ember.ObjectController.extend({
	actions: {
		setColor: function(color,img){
			App.newProduct.set('color',color);
			App.productPreview.set('front_image',img);
		},
		saveProduct: function(){
			var self = this;
			App.newProduct.save().then(function(product){
				 self.transitionToRoute('product-details',product);
			})
		}
	}
});


App.ChangeProductController = Ember.ObjectController.extend({
	filter: 1,

	filteredContent: function(){
		var filter = this.get('filter');
		var rx = new RegExp(filter, 'gi');
		var garments = this.get('arrangedContent');

		return garments.filter(function(garment) {
			return garment.get('product_category_id') == filter;
		});

	}.property('arrangedContent', 'filter'),
	actions: {
		setCategory: function(id){
			$('#product-selector li').removeClass('active');
			$(event.target).parent('li').addClass('active');
			this.set('filter',id);
		},
		setProduct: function(id){
			App.newProduct.set('garment_id',id);
			this.transitionToRoute('/');
		}
	}
});

App.ProductDetailsController = Ember.ObjectController.extend({
	sides: false,
	color: function(){
		model = this.get('content');
		return model.get('color');
	}.property('content'),
	quote: function(){
		model = this.get('content');
		if(this.get('sides') == 2){
			return model.get('print_2');
		}else{
			return model.get('print_1');
		}
	}.property('content','sides'),
	actions: {
		updateSides: function(){
			var self = this;
			setTimeout(function() {
				var count = $('.print-sides .side.active').length;
				self.set('sides',count);
			}, 250);
		},
		saveProduct: function(){
			var self = this;
			App.newProduct.save().then(function(product){
				 self.transitionToRoute('thank-you');
			})
		}
	}
});